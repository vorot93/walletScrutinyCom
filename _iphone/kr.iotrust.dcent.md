---
wsId: dCent
title: D’CENT Wallet
altTitle: 
authors:
- danny
appId: kr.iotrust.dcent
appCountry: kr
idd: 1447206611
released: 2019-01-26
updated: 2023-05-18
version: 5.24.2
stars: 3.9
reviews: 65
size: '57693184'
website: https://dcentwallet.com/
repository: 
issue: 
icon: kr.iotrust.dcent.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-10
signer: 
reviewArchive: 
twitter: DCENTwallets
social:
- https://www.facebook.com/DcentWalletGlobal
- https://github.com/DcentWallet
features: 
developerName: IoTrust Co., Ltd

---

{% include copyFromAndroid.html %}