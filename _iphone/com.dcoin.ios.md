---
wsId: dcoin
title: Dcoin - Bitcoin Exchange
altTitle: 
authors:
- danny
appId: com.dcoin.ios
appCountry: us
idd: 1508064925
released: 2018-12-20
updated: 2023-04-11
version: 5.7.2
stars: 3.2
reviews: 19
size: '64526336'
website: https://www.dcoin.com/
repository: 
issue: 
icon: com.dcoin.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive: 
twitter: dcoinexchange
social:
- https://www.linkedin.com/company/dcoin-exchange
features: 
developerName: DAVOX TECH PTE. LTD.

---

 {% include copyFromAndroid.html %}
