---
wsId: finq
title: Finq
altTitle: 
authors:
- danny
appId: com.trader.finq
appCountry: in
idd: '1280873475'
released: 2017-09-23
updated: 2019-12-19
version: '2.8'
stars: 2.6
reviews: 38
size: '103240704'
website: http://www.finq.com
repository: 
issue: 
icon: com.trader.finq.jpg
bugbounty: 
meta: obsolete
verdict: nosendreceive
date: 2022-07-11
signer: 
reviewArchive: 
twitter: finqcom
social: 
features: 
developerName: Leadcapital Corp LTD

---

{% include copyFromAndroid.html %}
