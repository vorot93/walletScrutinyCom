---
wsId: deribitOptions
title: 'Deribit: BTC Options & Futures'
altTitle: 
authors:
- danny
appId: com.deribit
appCountry: gb
idd: '1293674041'
released: 2017-11-17
updated: 2023-05-30
version: 3.2.0
stars: 5
reviews: 2
size: '34192384'
website: http://www.deribit.com
repository: 
issue: 
icon: com.deribit.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-31
signer: 
reviewArchive: 
twitter: deribitexchange
social:
- https://www.linkedin.com/company/deribit/
- https://www.reddit.com/r/DeribitExchange/
- https://t.me/deribit
- https://www.youtube.com/channel/UCbHKjlFogkOD0lUVeb5CsGA
features: 
developerName: Deribit

---

{% include copyFromAndroid.html %}