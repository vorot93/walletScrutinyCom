---
wsId: AdmiralMarkets
title: 'Admirals: Learn, Invest, Trade'
altTitle: 
authors:
- danny
appId: com.admiralmarkets.tradersroom
appCountry: us
idd: 1222861799
released: 2017-06-28
updated: 2023-06-08
version: 5.17.8
stars: 4.7
reviews: 25
size: '93649920'
website: https://admiralmarkets.com/
repository: 
issue: 
icon: com.admiralmarkets.tradersroom.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-16
signer: 
reviewArchive: 
twitter: AdmiralsGlobal
social:
- https://www.linkedin.com/company/-admiral-markets-group
- https://www.facebook.com/AdmiralsGlobal
features: 
developerName: Admiral Markets AS

---

{% include copyFromAndroid.html %}

