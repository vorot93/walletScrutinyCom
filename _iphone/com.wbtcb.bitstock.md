---
wsId: bitPlusbyWBTCb
title: Bit.plus by wBTCb
altTitle: 
authors:
- danny
appId: com.wbtcb.bitstock
appCountry: us
idd: '1508577020'
released: 2020-06-22
updated: 2023-05-22
version: 2.6.0
stars: 5
reviews: 1
size: '69480448'
website: https://bit.plus
repository: 
issue: 
icon: com.wbtcb.bitstock.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-04-13
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: IP wBTCB solutions, s.r.o.

---

{% include copyFromAndroid.html %}

