---
wsId: CoinCome
title: COINCOME - Cryptowallet&Rebate
altTitle: 
authors:
- danny
appId: io.cimcome.app
appCountry: bm
idd: 1536525077
released: 2020-12-09
updated: 2023-06-08
version: 2.1.1
stars: 0
reviews: 0
size: '73391104'
website: https://cimcome.sg
repository: 
issue: 
icon: io.cimcome.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-11-22
signer: 
reviewArchive: 
twitter: cimcome
social: 
features: 
developerName: Makers Farm Pte. Ltd.

---

{% include copyFromAndroid.html %}