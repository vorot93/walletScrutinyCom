---
wsId: easycoinExchange
title: EasyCoins
altTitle: 
authors:
- danny
appId: com.easycoin.exchange
appCountry: in
idd: '1617986309'
released: 2022-09-28
updated: 2023-05-24
version: 1.0.72
stars: 5
reviews: 2
size: '106365952'
website: https://www.easycoins.com/
repository: 
issue: 
icon: com.easycoin.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-31
signer: 
reviewArchive: 
twitter: EasyCoinsCrypto
social:
- https://www.instagram.com/easycoinscryptoglobal/
- https://www.reddit.com/r/EasyCoins/
- https://t.me/easycoins888
features: 
developerName: Easycoin Labs Private Limited

---

{% include copyFromAndroid.html %}