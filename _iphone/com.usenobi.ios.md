---
wsId: usenobi
title: 'NOBI: Buy & Invest Bitcoin'
altTitle: 
authors:
- danny
appId: com.usenobi.ios
appCountry: id
idd: '1510269362'
released: 2020-07-15
updated: 2023-05-25
version: 3.6.0
stars: 4.8
reviews: 69
size: '58093568'
website: https://usenobi.com
repository: 
issue: 
icon: com.usenobi.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-12
signer: 
reviewArchive: 
twitter: usenobi
social:
- https://www.facebook.com/usenobi
- https://www.instagram.com/usenobi
- https://t.me/usenobi
- https://www.youtube.com/c/usenobi
- https://discord.com/invite/hNGkFeQHQD
features: 
developerName: Nobi

---

{% include copyFromAndroid.html %}

