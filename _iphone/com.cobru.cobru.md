---
wsId: cobru
title: Cobru
altTitle: 
authors:
- danny
appId: com.cobru.cobru
appCountry: us
idd: '1574045983'
released: 2021-06-29
updated: 2023-04-25
version: 4.0.92
stars: 5
reviews: 1
size: '56411136'
website: https://cobru.co
repository: 
issue: 
icon: com.cobru.cobru.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-30
signer: 
reviewArchive: 
twitter: cobruapp
social:
- https://www.facebook.com/cobruapp
- https://www.instagram.com/cobruapp/
features: 
developerName: Cobru S.A.S.

---

{% include copyFromAndroid.html %}
