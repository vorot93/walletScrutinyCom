---
wsId: Afriex
title: Afriex - Money transfer
altTitle: 
authors:
- danny
appId: com.afriex.afriex
appCountry: us
idd: 1492022568
released: 2020-03-06
updated: 2023-05-24
version: 11.91.8
stars: 4.3
reviews: 1240
size: '75957248'
website: https://afriexapp.com
repository: 
issue: 
icon: com.afriex.afriex.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-30
signer: 
reviewArchive: 
twitter: afriexapp
social:
- https://www.linkedin.com/company/afriex
- https://www.facebook.com/AfriexApp
features: 
developerName: Afriex Inc

---

{% include copyFromAndroid.html %}
