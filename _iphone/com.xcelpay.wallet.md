---
wsId: XcelPay
title: XcelPay Crypto Bitcoin Wallet
altTitle: 
authors:
- leo
appId: com.xcelpay.wallet
appCountry: 
idd: 1461215417
released: 2019-05-26
updated: 2023-06-09
version: 2.78.1
stars: 4.3
reviews: 56
size: '43331584'
website: http://xcelpay.io
repository: 
issue: 
icon: com.xcelpay.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive: 
twitter: XcelPayWallet
social:
- https://www.linkedin.com/company/in/xcelpaywallet
- https://www.facebook.com/xcelpay
features: 
developerName: XcelPay Limited

---

This wallet has no claim of being non-custodial in the app's description.

The one-star ratings over and over tell:

* there is a referral program, promising rewards
* the rewards are never reflected in the wallet
* funds cannot be sent to a different wallet
* SCAM

As a probably custodial app, it is **not verifiable**.
