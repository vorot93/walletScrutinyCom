---
wsId: talkPlus
title: Talk+ Send Crypto with Friends
altTitle: 
authors:
- danny
appId: org.talkapp
appCountry: hk
idd: '1547227377'
released: 2021-02-10
updated: 2023-05-23
version: 2.22.6
stars: 4.5
reviews: 159
size: '113303552'
website: 
repository: 
issue: 
icon: org.talkapp.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-06-01
signer: 
reviewArchive: 
twitter: 
social:
- https://talkapp.org
features: 
developerName: BULL.B TECHNOLOGY LIMITED

---

{% include copyFromAndroid.html %}