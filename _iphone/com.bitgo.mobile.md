---
wsId: bitGo
title: BitGo
altTitle: 
authors:
- danny
appId: com.bitgo.mobile
appCountry: us
idd: '1608937235'
released: 2022-05-09
updated: 2022-11-02
version: 1.5.0
stars: 3.5
reviews: 6
size: '51440640'
website: https://bitgo.com
repository: 
issue: 
icon: com.bitgo.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-05-12
signer: 
reviewArchive: 
twitter: BitGo
social:
- https://www.youtube.com/channel/UC7ILbUGTCM83sdilLB8Qlmg
- https://www.linkedin.com/company/bitgo-inc
features: 
developerName: BitGo

---

{% include copyFromAndroid.html %}
