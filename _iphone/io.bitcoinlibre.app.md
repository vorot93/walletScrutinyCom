---
wsId: BitcoinLibre
title: Bitcoin Libre
altTitle: 
authors:
- danny
appId: io.bitcoinlibre.app
appCountry: us
idd: 1590680702
released: 2021-10-27
updated: 2023-05-12
version: 3.3.7
stars: 4.4
reviews: 419
size: '32292864'
website: http://bitcoinlibre.io/
repository: 
issue: 
icon: io.bitcoinlibre.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-11-11
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: BIZAN BIZ SA

---

{% include copyFromAndroid.html %}
