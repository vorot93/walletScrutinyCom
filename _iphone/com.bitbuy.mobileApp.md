---
wsId: bitbuy
title: 'Bitbuy: Buy Bitcoin Canada'
altTitle: 
authors:
- danny
appId: com.bitbuy.mobileApp
appCountry: ca
idd: 1476837869
released: 2019-10-21
updated: 2023-05-29
version: 4.6.1
stars: 4.6
reviews: 6541
size: '25900032'
website: https://bitbuy.ca/
repository: 
issue: 
icon: com.bitbuy.mobileApp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive: 
twitter: bitbuy
social:
- https://www.linkedin.com/company/bitbuyca
- https://www.facebook.com/bitbuyCA
features: 
developerName: Bitbuy Inc

---

 {% include copyFromAndroid.html %}
