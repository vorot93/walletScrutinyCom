---
wsId: coin98
title: Coin98 Super App
altTitle: 
authors:
- danny
appId: coin98.crypto.finance.insights
appCountry: us
idd: 1561969966
released: 2021-05-14
updated: 2023-05-30
version: 12.11.5
stars: 4.6
reviews: 596
size: '129324032'
website: https://coin98.com
repository: 
issue: 
icon: coin98.crypto.finance.insights.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-10
signer: 
reviewArchive: 
twitter: coin98_wallet
social:
- https://www.facebook.com/Coin98Wallet
features: 
developerName: Coin98 Finance

---

{% include copyFromAndroid.html %}