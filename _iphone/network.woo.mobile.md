---
wsId: wooXTrading
title: 'WOO X: Zero-fee crypto trading'
altTitle: 
authors:
- danny
appId: network.woo.mobile
appCountry: ph
idd: '1576648404'
released: 2021-09-17
updated: 2023-05-19
version: 3.4.2
stars: 5
reviews: 2
size: '68855808'
website: https://woo.org/
repository: 
issue: 
icon: network.woo.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-15
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Wootech Limited

---

{% include copyFromAndroid.html %}
