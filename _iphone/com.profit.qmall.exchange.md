---
wsId: qmallExchange
title: Qmall - єдина українська біржа
altTitle: 
authors:
- danny
appId: com.profit.qmall.exchange
appCountry: gb
idd: '1600467380'
released: 2021-12-21
updated: 2023-05-24
version: 1.1.42
stars: 5
reviews: 8
size: '24390656'
website: https://qmall.io/
repository: 
issue: 
icon: com.profit.qmall.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-02-25
signer: 
reviewArchive: 
twitter: QmallExchange
social:
- https://www.linkedin.com/company/qmallexchange/about/
- https://www.facebook.com/qmall.io
- https://www.youtube.com/channel/UCRfKNWczZ84ASCSEIsaLCag
features: 
developerName: QMALL Exchange

---

{% include copyFromAndroid.html %}
