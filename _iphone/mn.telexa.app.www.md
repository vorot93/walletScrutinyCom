---
wsId: mnTelexa
title: Telexa.mn - Хөрөнгө оруулалт
altTitle: 
authors:
- danny
appId: mn.telexa.app.www
appCountry: mn
idd: '1596968900'
released: 2021-12-02
updated: 2023-06-06
version: '6.1'
stars: 4.8
reviews: 2166
size: '54844416'
website: https://www.telexa.mn/
repository: 
issue: 
icon: mn.telexa.app.www.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-04-15
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/Telexa.mn/
features: 
developerName: Save Inc.

---

{% include copyFromAndroid.html %}
