---
wsId: CAPITALIKA
title: Capitalika
altTitle: 
authors: 
appId: com.capitalika.exchange
appCountry: ec
idd: '1570799130'
released: '2021-10-04'
updated: 2022-06-08
version: 1.0.9
stars: 4.6
reviews: 7
size: '7054336'
website: https://capitalika.com/
repository: 
issue: 
icon: com.capitalika.exchange.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-06-03
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Capitalika

---

{% include copyFromAndroid.html %}
