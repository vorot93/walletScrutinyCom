---
wsId: bitkeep
title: 'BitKeep: DeFi Wallet'
altTitle: 
authors:
- leo
appId: com.bitkeep.os
appCountry: 
idd: 1395301115
released: 2018-09-26
updated: 2023-04-27
version: 7.3.4
stars: 4.3
reviews: 306
size: '135846912'
website: https://bitkeep.com
repository: 
issue: 
icon: com.bitkeep.os.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: BitKeepOS
social:
- https://www.facebook.com/bitkeep
- https://github.com/bitkeepcom
features: 
developerName: BitKeep Global Inc.

---

 {% include copyFromAndroid.html %}
