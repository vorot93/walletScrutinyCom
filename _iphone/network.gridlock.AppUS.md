---
wsId: GridLock
title: Gridlock Crypto & NFT Wallet
altTitle: 
authors:
- danny
appId: network.gridlock.AppUS
appCountry: us
idd: '1567057330'
released: '2021-06-07'
updated: 2023-05-22
version: 2.0.7
stars: 4.7
reviews: 15
size: '73784320'
website: https://gridlock.network
repository: 
issue: 
icon: network.gridlock.AppUS.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-12-08
signer: 
reviewArchive: 
twitter: GridlockCrypto
social:
- https://www.facebook.com/GridlockNetwork
features: 
developerName: Gridlock

---

{% include copyFromAndroid.html %}