---
wsId: anycoinCZ
title: 'Anycoin.cz: Crypto exchange'
altTitle: 
authors:
- danny
appId: cz.anycoin.mobile
appCountry: cz
idd: '1616670336'
released: 2022-04-25
updated: 2023-05-09
version: 1.23.0
stars: 4.3
reviews: 35
size: '32768000'
website: https://www.anycoin.cz
repository: 
issue: 
icon: cz.anycoin.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-05-03
signer: 
reviewArchive: 
twitter: anycoin_cz
social:
- https://www.facebook.com/anycoinCZ
features: 
developerName: MP Developers s.r.o.

---

{% include copyFromAndroid.html %}