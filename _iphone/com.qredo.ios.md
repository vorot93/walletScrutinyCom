---
wsId: qredoNetwork
title: 'Qredo Network: Signing App'
altTitle: 
authors:
- danny
appId: com.qredo.ios
appCountry: us
idd: '1515898075'
released: 2020-07-08
updated: 2023-05-25
version: 1.2.2
stars: 4.3
reviews: 10
size: '41220096'
website: http://www.qredo.com/
repository: 
issue: 
icon: com.qredo.ios.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-04-11
signer: 
reviewArchive: 
twitter: QredoNetwork
social:
- https://www.linkedin.com/company/qredo/
features: 
developerName: Qredo Limited

---

{% include copyFromAndroid.html %}

