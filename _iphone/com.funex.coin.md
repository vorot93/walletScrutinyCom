---
wsId: funexCrypto
title: 'Funex: Crypto & Bitcoin Wallet'
altTitle: 
authors:
- danny
appId: com.funex.coin
appCountry: us
idd: '1617996833'
released: 2022-04-18
updated: 2023-05-12
version: 1.0.16
stars: 0
reviews: 0
size: '66777088'
website: 
repository: 
issue: 
icon: com.funex.coin.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-05-25
signer: 
reviewArchive: 
twitter: Funexclub
social:
- https://funexcoin.com
- https://www.facebook.com/officialfunexcoin
- https://t.me/Funex_official
- https://www.youtube.com/c/FunexClub
features: 
developerName: FUNEX BLOCKCHAIN SERVICES PRIVATE LIMITED

---

{% include copyFromAndroid.html %}
