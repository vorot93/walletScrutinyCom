---
wsId: eversend
title: Eversend - the money app
altTitle: 
authors:
- danny
appId: com.eversendapp
appCountry: lv
idd: 1438341192
released: 2020-05-28
updated: 2023-06-12
version: 0.3.90
stars: 0
reviews: 0
size: '95697920'
website: http://www.eversend.co
repository: 
issue: 
icon: com.eversendapp.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-11-01
signer: 
reviewArchive: 
twitter: eversendapp
social:
- https://www.linkedin.com/company/eversend
- https://www.facebook.com/eversendapp
features: 
developerName: Eversend

---

{% include copyFromAndroid.html %}
