---
wsId: kiteFinancialWallet
title: Kite Financial Wallet
altTitle: 
authors:
- danny
appId: io.kitefinancial.bitcoin
appCountry: us
idd: '1524242232'
released: 2020-07-21
updated: 2022-10-04
version: 2.2.7
stars: 3.3
reviews: 28
size: '61706240'
website: 
repository: 
issue: 
icon: io.kitefinancial.bitcoin.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-15
signer: 
reviewArchive: 
twitter: kite_financial
social:
- https://www.facebook.com/kitefinancial
- https://www.instagram.com/kite_financial
- https://www.linkedin.com/company/kitefinancial
features: 
developerName: 

---

{% include copyFromAndroid.html %}

