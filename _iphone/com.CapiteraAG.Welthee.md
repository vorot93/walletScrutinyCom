---
wsId: weltheeWallet
title: Welthee Wallet
altTitle: 
authors:
- danny
appId: com.CapiteraAG.Welthee
appCountry: sg
idd: '1562108720'
released: 2021-07-19
updated: 2023-05-26
version: 4.4.0
stars: 0
reviews: 0
size: '96216064'
website: https://welthee.com
repository: 
issue: 
icon: com.CapiteraAG.Welthee.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-04-13
signer: 
reviewArchive: 
twitter: Welthee
social:
- https://www.linkedin.com/company/welthee
features: 
developerName: Capitera AG

---

{% include copyFromAndroid.html %}

