---
wsId: JPEX
title: JPEX Wallet
altTitle: 
authors:
- danny
appId: io.jp-ex.iosapp2
appCountry: jp
idd: 1559708728
released: 2021-04-22
updated: 2023-06-07
version: 2.79.826
stars: 2
reviews: 1
size: '88273920'
website: https://jp-ex.io/
repository: 
issue: 
icon: io.jp-ex.iosapp2.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: JP-EX CRYPTO ASSET PLATFORM PTY LTD

---

{% include copyFromAndroid.html %}

