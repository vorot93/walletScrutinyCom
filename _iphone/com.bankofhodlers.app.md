---
wsId: Vauld
title: Vauld
altTitle: 
authors:
- kiwilamb
appId: com.bankofhodlers.app
appCountry: 
idd: 1509251174
released: 2020-05-12
updated: 2023-04-25
version: 2.8.1
stars: 4
reviews: 297
size: '89300992'
website: https://www.vauld.com/
repository: 
issue: 
icon: com.bankofhodlers.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-08
signer: 
reviewArchive: 
twitter: Vauld_
social:
- https://www.linkedin.com/company/vauld
- https://www.facebook.com/VauldOfficial
- https://www.reddit.com/r/BankofHodlers
features: 
developerName: DEFI TECHNOLOGIES PTE. LTD.

---

The Vauld website Help Center had an article "Security at Vauld" which covers a number of security risk.<br>
A statement of the management of the users "funds" makes it pretty clear the wallets private keys are in control of the provider.

> Our funds are managed through a multi signature system with the signatories being our co-founders.

Our verdict: This wallet is custodial and therefore **not verifiable**.
