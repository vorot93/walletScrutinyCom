---
wsId: nowWallet
title: 'NOW Wallet: Buy & Swap Bitcoin'
altTitle: 
authors:
- danny
appId: io.changenow.wallet-now
appCountry: us
idd: '1591216386'
released: 2021-10-23
updated: 2023-06-01
version: 3.6.1
stars: 4.5
reviews: 296
size: '103621632'
website: https://walletnow.app
repository: 
issue: 
icon: io.changenow.wallet-now.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-04-08
signer: 
reviewArchive: 
twitter: NOW_Wallet
social:
- https://www.facebook.com/ChangeNOW.io/
- https://t.me/NOWWallet_channel
- https://www.reddit.com/r/ChangeNOW_io/
features: 
developerName: CHN Group Limited

---

{% include copyFromAndroid.html %}

