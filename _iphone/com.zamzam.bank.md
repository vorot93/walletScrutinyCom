---
wsId: ZamZam
title: Zamzam – money transfers
altTitle: 
authors:
- danny
appId: com.zamzam.bank
appCountry: ru
idd: 1521900439
released: 2020-07-04
updated: 2023-05-06
version: 1.10.4
stars: 3.8
reviews: 84
size: '54175744'
website: https://zam.me
repository: 
issue: 
icon: com.zamzam.bank.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-30
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: ZAMZAMTECHNOLOGY OU

---

{% include copyFromAndroid.html %}