---
wsId: WalletofSatoshi
title: Wallet of Satoshi
altTitle: 
authors:
- leo
appId: com.livingroomofsatoshi.wallet
appCountry: 
idd: 1438599608
released: 2019-05-20
updated: 2023-06-06
version: 2.2.2
stars: 4.6
reviews: 32
size: '54321152'
website: https://www.walletofsatoshi.com
repository: 
issue: 
icon: com.livingroomofsatoshi.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive: 
twitter: walletofsatoshi
social:
- https://www.facebook.com/walletofsatoshi
features:
- ln
developerName: Living Room of Satoshi Pty Ltd

---

This is a custodial wallet according to their website's FAQ:

> It is a zero-configuration custodial wallet with a focus on simplicity and the
  best possible user experience.

and therefore **not verifiable**.
