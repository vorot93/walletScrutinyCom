---
wsId: yappi
title: Yappi
altTitle: 
authors:
- danny
appId: br.com.yappi.app
appCountry: br
idd: '1543965460'
released: 2021-02-05
updated: 2022-11-12
version: 1.2.1
stars: 3.7
reviews: 45
size: '34829312'
website: https://yappi.com.br/
repository: 
issue: 
icon: br.com.yappi.app.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-03-07
signer: 
reviewArchive: 
twitter: yappi_br
social:
- https://www.facebook.com/yappi.br
- https://www.instagram.com/yappi.br/
- https://www.linkedin.com/company/yappi-br/
features: 
developerName: Yappi Software e Serviços LTDA

---

{% include copyFromAndroid.html %}
