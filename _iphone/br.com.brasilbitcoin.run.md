---
wsId: brasilBitcoin
title: 'Brasil Bitcoin: Cripto Grátis'
altTitle: 
authors:
- danny
appId: br.com.brasilbitcoin.run
appCountry: br
idd: 1519300849
released: 2020-07-27
updated: 2023-04-12
version: 2.9.13
stars: 4.6
reviews: 1845
size: '94938112'
website: 
repository: 
issue: 
icon: br.com.brasilbitcoin.run.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive: 
twitter: brbtcoficial
social:
- https://www.facebook.com/brbtcoficial
features: 
developerName: Brasil Bitcoin Servicos Digitais LTDA

---

{% include copyFromAndroid.html %}
