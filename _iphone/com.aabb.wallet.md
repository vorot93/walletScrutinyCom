---
wsId: AABBWallet
title: AABB Wallet
altTitle: 
authors:
- danny
appId: com.aabb.wallet
appCountry: ca
idd: 1557298954
released: 2021-03-14
updated: 2023-05-29
version: 1.0.256
stars: 4.9
reviews: 99
size: '33128448'
website: https://aabbgoldtoken.com/
repository: 
issue: 
icon: com.aabb.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-26
signer: 
reviewArchive: 
twitter: AsiaBroadband
social: 
features: 
developerName: Asia Broadband, Inc.

---

{% include copyFromAndroid.html %}
