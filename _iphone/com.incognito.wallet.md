---
wsId: incognito
title: Incognito crypto wallet
altTitle: 
authors:
- leo
appId: com.incognito.wallet
appCountry: 
idd: 1475631606
released: 2019-08-21
updated: 2023-04-29
version: 5.10.0
stars: 3.8
reviews: 111
size: '56088576'
website: https://incognito.org
repository: https://github.com/incognitochain/incognito-wallet
issue: 
icon: com.incognito.wallet.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2021-10-01
signer: 
reviewArchive: 
twitter: incognitochain
social: 
features: 
developerName: Incognito Core Team LLC

---

 {% include copyFromAndroid.html %}
