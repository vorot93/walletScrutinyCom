---
wsId: btcboxJP
title: BTCBOXビットコイン取引所
altTitle: 
authors:
- danny
appId: btctrade.btcbox
appCountry: jp
idd: '1076075645'
released: 2016-01-22
updated: 2023-06-01
version: '1.217'
stars: 2.6
reviews: 14
size: '13716480'
website: http://www.btcbox.co.jp
repository: 
issue: 
icon: btctrade.btcbox.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-05-04
signer: 
reviewArchive: 
twitter: BtcboxE
social:
- https://www.facebook.com/btcbox/
features: 
developerName: BTCBOX CO.,LTD

---

{% include copyFromAndroid.html %}
