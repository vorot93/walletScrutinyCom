---
wsId: OKEx
title: 'OKX: Buy Bitcoin, ETH, Crypto'
altTitle: 
authors:
- leo
appId: com.okex.OKExAppstoreFull
appCountry: 
idd: 1327268470
released: 2018-01-04
updated: 2023-06-13
version: 6.19.0
stars: 4.8
reviews: 13854
size: '531899392'
website: https://www.okx.com/download
repository: 
issue: 
icon: com.okex.OKExAppstoreFull.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2020-12-23
signer: 
reviewArchive: 
twitter: OKEx
social:
- https://www.facebook.com/okexofficial
- https://www.reddit.com/r/OKEx
features: 
developerName: OKEX MALTA LTD

---

On their website we find:

> **Institutional-grade Security**<br>
  Cold wallet technology developed by the world's top security team adopts a
  multi-security-layer mechanism to safeguard your assets

"Cold wallet technology" means this is a custodial offering and therefore
**not verifiable**.
