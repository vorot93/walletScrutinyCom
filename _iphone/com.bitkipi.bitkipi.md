---
wsId: bitKipi
title: Bitkipi
altTitle: 
authors:
- danny
appId: com.bitkipi.bitkipi
appCountry: gb
idd: '1600263918'
released: 2021-12-26
updated: 2022-10-02
version: 1.5.0
stars: 0
reviews: 0
size: '27810816'
website: https://bitkipi.com/
repository: 
issue: 
icon: com.bitkipi.bitkipi.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-05-12
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Kipi Technologies Sàrl

---

{% include copyFromAndroid.html %}