---
wsId: BBAEPro
title: BBAE
altTitle: 
authors:
- danny
appId: com.bbae.anno
appCountry: ca
idd: '1123918627'
released: 2016-06-25
updated: 2022-10-28
version: 5.0.4
stars: 4
reviews: 4
size: '138987520'
website: https://www.bbae.com
repository: 
issue: 
icon: com.bbae.anno.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-04-20
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: 

---

{% include copyFromAndroid.html %}

