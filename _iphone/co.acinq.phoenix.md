---
wsId: phoenix
title: Phoenix Wallet
altTitle: 
authors:
- leo
- danny
appId: co.acinq.phoenix
appCountry: us
idd: 1544097028
released: 2021-07-13
updated: 2023-04-28
version: 1.6.1
stars: 4.3
reviews: 21
size: '44178432'
website: https://phoenix.acinq.co
repository: https://github.com/ACINQ/phoenix-kmm
issue: https://github.com/ACINQ/phoenix/issues/112
icon: co.acinq.phoenix.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2023-04-15
signer: 
reviewArchive: 
twitter: PhoenixWallet
social: 
features:
- ln
developerName: ACINQ

---

{% include copyFromAndroid.html %}
