---
wsId: BitstockBitstart
title: ビットスタート ビットコインをもらって、仮想通貨を学習・運用
altTitle: 
authors:
- danny
appId: jp.paddleinc.bitstock
appCountry: jp
idd: 1436815668
released: 2018-11-02
updated: 2023-04-25
version: 1.4.56
stars: 4.1
reviews: 39590
size: '120140800'
website: http://www.paddle-inc.jp/
repository: 
issue: 
icon: jp.paddleinc.bitstock.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-01
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Paddle.inc

---

{% include copyFromAndroid.html %}
