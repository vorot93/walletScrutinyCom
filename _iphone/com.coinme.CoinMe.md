---
wsId: coinme
title: 'Coinme: Buy Bitcoin With Cash'
altTitle: 
authors:
- danny
appId: com.coinme.CoinMe
appCountry: us
idd: 1545440300
released: 2021-05-11
updated: 2023-05-30
version: 2.1.4
stars: 4.6
reviews: 3681
size: '111524864'
website: https://coinme.com/
repository: 
issue: 
icon: com.coinme.CoinMe.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: Coinme
social:
- https://www.linkedin.com/company/coinme
- https://www.facebook.com/Coinme
features: 
developerName: Coinme Inc.

---

 {% include copyFromAndroid.html %}
