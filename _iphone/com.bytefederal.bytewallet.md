---
wsId: byteWallet
title: ByteWallet
altTitle: 
authors:
- danny
appId: com.bytefederal.bytewallet
appCountry: us
idd: '1569062610'
released: 2021-07-27
updated: 2023-04-27
version: 1.4.3
stars: 3.5
reviews: 31
size: '80871424'
website: https://www.bytefederal.com/
repository: 
issue: 
icon: com.bytefederal.bytewallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-05-05
signer: 
reviewArchive: 
twitter: bytefederal
social:
- https://www.facebook.com/bytefederal
- https://www.instagram.com/bytefederalatm
- https://www.youtube.com/channel/UCozOzfZ0MgqLT_TA7hbNh4g
- https://www.linkedin.com/company/bytefederal
features: 
developerName: Byte Federal, Inc.

---

{% include copyFromAndroid.html %}
