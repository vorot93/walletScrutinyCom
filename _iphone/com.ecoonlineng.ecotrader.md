---
wsId: ecoTrader
title: Eco Trader
altTitle: 
authors:
- danny
appId: com.ecoonlineng.ecotrader
appCountry: us
idd: '1535823080'
released: 2020-11-05
updated: 2021-10-31
version: 3.7.0
stars: 3.6
reviews: 36
size: '71578624'
website: 
repository: 
issue: 
icon: com.ecoonlineng.ecotrader.jpg
bugbounty: 
meta: stale
verdict: nowallet
date: 2023-05-23
signer: 
reviewArchive: 
twitter: 
social:
- https://ecoonlineng.com/
features: 
developerName: Uche Shadrach Onuigbo

---

{% include copyFromAndroid.html %}