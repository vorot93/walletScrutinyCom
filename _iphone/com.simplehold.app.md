---
wsId: simpleholdCrypto
title: SimpleHold - Crypto Wallet
altTitle: 
authors:
- danny
appId: com.simplehold.app
appCountry: gb
idd: '1589064973'
released: 2021-12-14
updated: 2023-02-23
version: '1.21'
stars: 1
reviews: 2
size: '66384896'
website: https://simplehold.io/
repository: 
issue: 
icon: com.simplehold.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-04-11
signer: 
reviewArchive: 
twitter: SimpleHold
social:
- https://t.me/simplehold_io
- https://www.reddit.com/r/SH_Cryptowallet/
features: 
developerName: Simple Swap LTD

---

{% include copyFromAndroid.html %}

