---
wsId: goabra
title: 'Abra: Buy & Trade BTC & Crypto'
altTitle: 
authors:
- leo
appId: com.goabra.abra
appCountry: 
idd: 966301394
released: 2015-03-12
updated: 2023-05-26
version: 144.0.0
stars: 4.6
reviews: 18560
size: '221257728'
website: 
repository: 
issue: 
icon: com.goabra.abra.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-01-04
signer: 
reviewArchive: 
twitter: AbraGlobal
social:
- https://www.linkedin.com/company/abra
- https://www.facebook.com/GoAbraGlobal
features: 
developerName: Plutus Financial

---

This is the iPhone version of the Android
{% include walletLink.html wallet='android/com.plutus.wallet' %}.

Just like the Android version, this wallet is **not verifiable**.
