---
wsId: sMiles
title: 'sMiles: Bitcoin Rewards'
altTitle: 
authors:
- danny
appId: inc.standapp.sMiles
appCountry: us
idd: 1492458803
released: 2020-12-18
updated: 2023-05-19
version: '7.0'
stars: 4.6
reviews: 2673
size: '128533504'
website: https://www.smilesbitcoin.com/
repository: 
issue: 
icon: inc.standapp.sMiles.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-04
signer: 
reviewArchive: 
twitter: smilesbitcoin
social:
- https://www.facebook.com/smilesbitcoin
features: 
developerName: Standapp inc.

---

{% include copyFromAndroid.html %}
