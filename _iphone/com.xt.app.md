---
wsId: xtcom
title: 'XT.com: Buy Bitcoin & Ethereum'
altTitle: 
authors:
- danny
appId: com.xt.app
appCountry: us
idd: '1556596708'
released: 2021-03-08
updated: 2023-05-22
version: 4.23.0
stars: 3.3
reviews: 236
size: '207145984'
website: https://www.xt.com
repository: 
issue: 
icon: com.xt.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-02-03
signer: 
reviewArchive: 
twitter: XTexchange
social:
- https://www.linkedin.com/company/xt-com-exchange/
- https://www.facebook.com/XT.comexchange
features: 
developerName: XT LTD, LLC

---

{% include copyFromAndroid.html %}
