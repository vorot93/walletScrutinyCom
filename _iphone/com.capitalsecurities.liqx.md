---
wsId: liquidityX
title: 'LiquidityX: CFD Online Trading'
altTitle: 
authors:
- danny
appId: com.capitalsecurities.liqx
appCountry: ch
idd: '1535931066'
released: 2021-10-13
updated: 2023-05-31
version: 2.5.4
stars: 0
reviews: 0
size: '111727616'
website: https://www.liquidityx.com/eu/
repository: 
issue: 
icon: com.capitalsecurities.liqx.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-05-13
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Capital Securities S.A.

---

{% include copyFromAndroid.html %}
