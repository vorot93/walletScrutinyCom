---
wsId: mwfreewallet
title: Freewallet Multi Crypto Wallet
altTitle: 
authors:
- leo
appId: mw.org.freewallet.app
appCountry: 
idd: 1274003898
released: 2017-09-01
updated: 2022-06-07
version: 1.16.10
stars: 3.9
reviews: 1371
size: '36701184'
website: https://freewallet.org
repository: 
issue: 
icon: mw.org.freewallet.app.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-06-03
signer: 
reviewArchive: 
twitter: freewalletorg
social:
- https://www.facebook.com/freewallet.org
features: 
developerName: Freewallet

---

{% include copyFromAndroid.html %}
