---
wsId: switchere
title: 'Switchere: Buy&Sell Crypto BTC'
altTitle: 
authors:
- danny
appId: com.switchere.switchere
appCountry: us
idd: '1550289857'
released: 2021-11-15
updated: 2023-01-06
version: 1.1.4
stars: 4
reviews: 116
size: '38531072'
website: https://switchere.com/
repository: 
issue: 
icon: com.switchere.switchere.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-12
signer: 
reviewArchive: 
twitter: Switchere_com
social:
- https://www.facebook.com/switchere.official
- https://t.me/switchere
- https://www.reddit.com/user/switchere
features: 
developerName: Fiteum OU

---

{% include copyFromAndroid.html %}

