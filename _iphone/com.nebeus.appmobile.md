---
wsId: nebeus
title: 'Nebeus: IBANs for Crypto'
altTitle: 
authors:
- danny
appId: com.nebeus.appmobile
appCountry: gt
idd: '1568251064'
released: 2021-05-26
updated: 2023-05-03
version: 1.3.35
stars: 0
reviews: 0
size: '106065920'
website: https://nebeus.com
repository: 
issue: 
icon: com.nebeus.appmobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-02-25
signer: 
reviewArchive: 
twitter: NebeusWorld
social:
- https://www.linkedin.com/company/nebeus/
features: 
developerName: Nebeus

---

{% include copyFromAndroid.html %}
