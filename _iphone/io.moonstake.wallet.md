---
wsId: Moonstake
title: Moonstake Wallet
altTitle: 
authors:
- danny
appId: io.moonstake.wallet
appCountry: us
idd: 1502532651
released: 2020-03-25
updated: 2023-05-30
version: 2.22.2
stars: 3.3
reviews: 16
size: '121630720'
website: http://moonstake.io
repository: 
issue: 
icon: io.moonstake.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: moonstake
social:
- https://www.linkedin.com/company/moonstake
- https://www.facebook.com/moonstakekorea
features: 
developerName: Moonstake Limited

---

{% include copyFromAndroid.html %}