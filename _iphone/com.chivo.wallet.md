---
wsId: ChivoWallet
title: Chivo Wallet
altTitle: 
authors:
- danny
appId: com.chivo.wallet
appCountry: sv
idd: 1581515981
released: 2021-09-07
updated: 2023-04-19
version: 2.3.0
stars: 2.6
reviews: 4053
size: '47480832'
website: https://www.chivowallet.com
repository: 
issue: 
icon: com.chivo.wallet.jpg
bugbounty: 
meta: ok
verdict: obfuscated
date: 2021-10-10
signer: 
reviewArchive: 
twitter: chivowallet
social:
- https://www.facebook.com/ChivoWalletSLV
features: 
developerName: Gobierno de El Salvador

---

{% include copyFromAndroid.html %}
