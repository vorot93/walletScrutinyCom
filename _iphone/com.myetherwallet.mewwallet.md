---
wsId: mewEthereum
title: 'MEW crypto wallet: DeFi Web3'
altTitle: 
authors:
- danny
appId: com.myetherwallet.mewwallet
appCountry: us
idd: 1464614025
released: 2020-03-12
updated: 2023-06-08
version: 2.3.4
stars: 4.7
reviews: 4642
size: '150710272'
website: http://mewwallet.com
repository: 
issue: 
icon: com.myetherwallet.mewwallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-01-10
signer: 
reviewArchive: 
twitter: myetherwallet
social:
- https://www.linkedin.com/company/myetherwallet
- https://www.facebook.com/MyEtherWallet
- https://www.reddit.com/r/MyEtherWallet
features: 
developerName: MyEtherWallet, Inc.

---

{% include copyFromAndroid.html %}