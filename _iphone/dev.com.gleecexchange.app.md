---
wsId: gleecExchange
title: Gleec Exchange
altTitle: 
authors: 
appId: dev.com.gleecexchange.app
appCountry: us
idd: '1541005487'
released: 2021-02-18
updated: 2021-02-18
version: '1.0'
stars: 0
reviews: 0
size: '22282240'
website: https://gleecbtc.com
repository: 
issue: 
icon: dev.com.gleecexchange.app.jpg
bugbounty: 
meta: obsolete
verdict: fewusers
date: 2023-04-22
signer: 
reviewArchive: 
twitter: GleecOfficial
social:
- https://www.facebook.com/gleecofficial
- https://t.me/officialgleecoin
features: 
developerName: Gleec-BTC OU

---

{% include copyFromAndroid.html %}

