---
wsId: bitcointrade
title: BitcoinTrade - Criptomoedas
altTitle: 
authors:
- danny
appId: com.root.BitcoinTrade
appCountry: br
idd: 1320032339
released: 2017-12-13
updated: 2023-06-12
version: 4.5.1
stars: 3.9
reviews: 923
size: '59240448'
website: http://www.bitcointrade.com.br/
repository: 
issue: 
icon: com.root.BitcoinTrade.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive: 
twitter: 
social:
- https://www.linkedin.com/company/bitcointrade
- https://www.facebook.com/BitcointradeBR
features: 
developerName: PeerTrade Digital

---

{% include copyFromAndroid.html %}
