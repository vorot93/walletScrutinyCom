---
wsId: pexpaypeaceful
title: 'Pexpay: 0 Fee BTC trading'
altTitle: 
authors:
- danny
appId: com.peaceful.pexpay.app.ios
appCountry: us
idd: '1618178873'
released: 2022-04-11
updated: 2023-05-30
version: 1.18.0
stars: 4
reviews: 10
size: '344730624'
website: https://www.pexpay.com
repository: 
issue: 
icon: com.peaceful.pexpay.app.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-11
signer: 
reviewArchive: 
twitter: Pexpay_official
social:
- https://www.facebook.com/pexpay.official/
- https://www.linkedin.com/company/pexpay/
- https://www.youtube.com/@pexpay
- https://vk.com/pexpayofficial
features: 
developerName: PEACEFUL INTERNATIONAL LIMITED

---

{% include copyFromAndroid.html %}

