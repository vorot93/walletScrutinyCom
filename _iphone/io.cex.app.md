---
wsId: cex
title: CEX.IO Cryptocurrency Exchange
altTitle: 
authors:
- kiwilamb
- leo
appId: io.cex.app
appCountry: 
idd: 1047225016
released: 2015-12-22
updated: 2023-06-07
version: 9.0.4
stars: 4.6
reviews: 5515
size: '81505280'
website: https://cex.io
repository: 
issue: 
icon: io.cex.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-04-19
signer: 
reviewArchive: 
twitter: cex_io
social:
- https://www.linkedin.com/company/cex-io
- https://www.facebook.com/CEX.IO
features: 
developerName: CEX.IO LTD

---

The CEX.io mobile app claims on the website to manage bitcoins...

> Stay in control of your funds anywhere. Deposit and withdraw crypto and fiat, add your debit or credit card in a few clicks, and store your funds securely.

however their is no evidence of the wallet being non-custodial, with no source code repository listed or found...

Our verdict: This 'wallet' is probably custodial but does not provide public source and therefore is **not verifiable**.
