---
wsId: FluxPay
title: Flux Pay
altTitle: 
authors:
- danny
appId: com.blueloopflux.app
appCountry: us
idd: 1534426282
released: 2020-10-15
updated: 2023-04-15
version: 3.0.391
stars: 3.5
reviews: 70
size: '59254784'
website: https://iflux.app/
repository: 
issue: 
icon: com.blueloopflux.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-24
signer: 
reviewArchive: 
twitter: ifluxdotapp
social:
- https://www.linkedin.com/company/iflux-pay
features: 
developerName: Blueloop

---

{% include copyFromAndroid.html %}
