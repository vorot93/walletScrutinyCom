---
wsId: paybankExchange
title: 24PayBank - buy Bitcoin
altTitle: 
authors:
- danny
appId: com.xchange.24paybank
appCountry: ru
idd: '1504034176'
released: 2020-03-24
updated: 2022-12-21
version: 1.1.2
stars: 4.5
reviews: 22
size: '72990720'
website: https://24paybank.net
repository: 
issue: 
icon: com.xchange.24paybank.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-04-22
signer: 
reviewArchive: 
twitter: 24Paybank
social: 
features: 
developerName: XChangeCapitalGroup

---

{% include copyFromAndroid.html %}

