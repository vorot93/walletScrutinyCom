---
wsId: cryptoXpress
title: CryptoXpress
altTitle: 
authors:
- danny
appId: com.cryptoxpress.mobile
appCountry: ph
idd: '1591792414'
released: 2021-11-18
updated: 2023-05-09
version: 1.0.11
stars: 0
reviews: 0
size: '40102912'
website: https://cryptoxpress.com/
repository: 
issue: 
icon: com.cryptoxpress.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-21
signer: 
reviewArchive: 
twitter: crypto_xpress
social:
- https://www.facebook.com/iamCryptoxpress/
features: 
developerName: cryptoxpress

---

{% include copyFromAndroid.html %}

