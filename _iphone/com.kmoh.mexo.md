---
wsId: Mexo
title: TruBit Pro | Buy Crypto Now
altTitle: 
authors:
- danny
appId: com.kmoh.mexo
appCountry: us
idd: 1555609032
released: 2021-03-01
updated: 2023-05-11
version: 3.2.9
stars: 4.9
reviews: 186
size: '160436224'
website: https://help.trubit.com/en
repository: 
issue: 
icon: com.kmoh.mexo.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: mexo_io
social:
- https://www.linkedin.com/company/mexoio
- https://www.facebook.com/mexo.io
features: 
developerName: TruBit Ltd.

---

{% include copyFromAndroid.html %}
