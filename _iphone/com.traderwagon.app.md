---
wsId: traderWagon
title: 'TraderWagon: Copy Trading'
altTitle: 
authors:
- danny
appId: com.traderwagon.app
appCountry: sg
idd: '1605104202'
released: 2022-02-21
updated: 2023-05-24
version: 1.7.1
stars: 5
reviews: 6
size: '112915456'
website: https://www.traderwagon.com/
repository: 
issue: 
icon: com.traderwagon.app.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-04-12
signer: 
reviewArchive: 
twitter: traderwagon
social: 
features: 
developerName: TraderWagon Ltd

---

{% include copyFromAndroid.html %}

