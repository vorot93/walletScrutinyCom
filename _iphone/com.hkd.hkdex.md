---
wsId: hkd
title: HKD.com
altTitle: 
authors:
- danny
appId: com.hkd.hkdex
appCountry: us
idd: '1522416988'
released: 2020-07-13
updated: 2023-05-18
version: 2.5.8
stars: 2.6
reviews: 32
size: '120650752'
website: http://hkd.com
repository: 
issue: 
icon: com.hkd.hkdex.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-02-13
signer: 
reviewArchive: 
twitter: HKD_exchange
social:
- https://www.facebook.com/hkdexchange/
- https://www.youtube.com/c/hkdexchange
- https://www.instagram.com/hkdexchange/?hl=en
features: 
developerName: Hong Kong Digital Asset Exchange Limited

---

{% include copyFromAndroid.html %}
