---
wsId: idax
title: 'iDAX: Crypto Exchange'
altTitle: 
authors:
- danny
appId: com.idax.exchange
appCountry: us
idd: '1619039755'
released: 2022-04-27
updated: 2023-04-27
version: 1.1.2
stars: 1
reviews: 2
size: '144814080'
website: https://www.idax.exchange
repository: 
issue: 
icon: com.idax.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-02-22
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/daxmnofficial
features: 
developerName: Ard Financial Group

---

{% include copyFromAndroid.html %}
