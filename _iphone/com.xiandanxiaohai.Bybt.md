---
wsId: 
title: Coinglass - Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.xiandanxiaohai.Bybt
appCountry: us
idd: 1522250001
released: 2020-07-08
updated: 2023-06-08
version: 1.6.7
stars: 4.9
reviews: 845
size: '19440640'
website: https://www.coinglass.com
repository: 
issue: 
icon: com.xiandanxiaohai.Bybt.jpg
bugbounty: 
meta: ok
verdict: fake
date: 2021-11-02
signer: 
reviewArchive: 
twitter: coinglass_com
social: 
features: 
developerName: Coinglass Technology Co., Limited

---

{% include copyFromAndroid.html %}
