---
wsId: blockbank.v2
title: blockbank
altTitle: 
authors:
- danny
appId: ai.blockbank.blockbankv2
appCountry: us
idd: '1592298073'
released: 2021-11-01
updated: 2023-04-26
version: 3.3.6
stars: 4.8
reviews: 13
size: '45182976'
website: https://twitter.com/BLOCKBANKapp
repository: 
issue: 
icon: ai.blockbank.blockbankv2.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-03-06
signer: 
reviewArchive: 
twitter: BLOCKBANKapp
social:
- https://www.facebook.com/blockbank
- https://www.linkedin.com/company/blockbankapp/
- https://www.youtube.com/channel/UC9OwNLa0vkHIRO77fPqAAJg
features: 
developerName: 

---

{% include copyFromAndroid.html %}