---
wsId: bitcointradingcapital
title: Bitcoin trading - Capital.com
altTitle: 
authors:
- danny
appId: com.kapital.trade.crypto
appCountry: cz
idd: 1487443266
released: 2019-11-26
updated: 2023-06-01
version: 1.61.3
stars: 4.7
reviews: 788
size: '88946688'
website: https://capital.com/
repository: 
issue: 
icon: com.kapital.trade.crypto.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-09-03
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: CAPITAL BULGARIA

---

{% include copyFromAndroid.html %}
