---
wsId: DopamineBitcoin
title: Dopamine - Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.programonks.CoinMarketApp
appCountry: us
idd: 1350234503
released: 2018-03-02
updated: 2023-05-17
version: 13.5.0
stars: 4.6
reviews: 609
size: '123805696'
website: https://www.dopamineapp.com/
repository: 
issue: 
icon: com.programonks.CoinMarketApp.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-10
signer: 
reviewArchive: 
twitter: mydopamineapp
social:
- https://www.facebook.com/myDopamineApp
features: 
developerName: CORTEX AG

---

{% include copyFromAndroid.html %}
