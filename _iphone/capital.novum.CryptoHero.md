---
wsId: CryptoHero
title: 'CryptoHero: Crypto Trading Bot'
altTitle: 
authors:
- danny
appId: capital.novum.CryptoHero
appCountry: us
idd: '1488574255'
released: 2019-12-18
updated: 2023-06-02
version: 2.2.0
stars: 3.5
reviews: 20
size: '92114944'
website: https://www.cryptohero.ai
repository: 
issue: 
icon: capital.novum.CryptoHero.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2022-07-14
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Novum Global Ventures Pte Ltd

---

{% include copyFromAndroid.html %}