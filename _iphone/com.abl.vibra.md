---
wsId: vibra
title: 'VIBRA Wallet: Buy BTC and ETH'
altTitle: 
authors:
- danny
appId: com.abl.vibra
appCountry: ng
idd: '1583907652'
released: 2021-10-24
updated: 2023-05-22
version: 1.6.81
stars: 4.6
reviews: 38
size: '65753088'
website: https://www.vibra.one/
repository: 
issue: 
icon: com.abl.vibra.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-02-03
signer: 
reviewArchive: 
twitter: VibraAfrica
social:
- https://www.linkedin.com/company/vibraafrica/
- https://www.youtube.com/channel/UC8ZcpYoBFSHgsbTQyQbutnA
- https://www.facebook.com/VibraAfrica
features: 
developerName: ABL Management Limited

---

{% include copyFromAndroid.html %}
