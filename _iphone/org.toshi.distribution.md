---
wsId: CoinbaseWallet
title: 'Coinbase Wallet: NFTs & Crypto'
altTitle: 
authors:
- leo
appId: org.toshi.distribution
appCountry: 
idd: 1278383455
released: 2017-09-27
updated: 2023-05-23
version: '28.44'
stars: 4.6
reviews: 125823
size: '84497408'
website: https://www.coinbase.com/wallet
repository: 
issue: 
icon: org.toshi.distribution.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-01-04
signer: 
reviewArchive: 
twitter: CoinbaseWallet
social: 
features: 
developerName: Coinbase Wallet

---

This is the iPhone version of the Android
{% include walletLink.html wallet='android/org.toshi' %}.

Just like the Android version, this wallet is **not verifiable**.
