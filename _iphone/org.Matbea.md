---
wsId: matbeaExchange
title: Matbea
altTitle: 
authors:
- danny
appId: org.Matbea
appCountry: ru
idd: '1518987729'
released: 2020-06-30
updated: 2023-04-18
version: 1.4.5
stars: 3.8
reviews: 111
size: '34920448'
website: 
repository: 
issue: 
icon: org.Matbea.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-07
signer: 
reviewArchive: 
twitter: 
social:
- https://t.me/matbea/
features: 
developerName: MATBEA LTD

---

{% include copyFromAndroid.html %}

