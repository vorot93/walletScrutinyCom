---
wsId: coinchange
title: Coinchange
altTitle: 
authors:
- danny
appId: io.coinchange
appCountry: us
idd: '1585629448'
released: 2021-11-09
updated: 2023-04-18
version: 2.0.2
stars: 4.2
reviews: 39
size: '149646336'
website: https://www.coinchange.io
repository: 
issue: 
icon: io.coinchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-02
signer: 
reviewArchive: 
twitter: coinchangeio
social:
- https://www.linkedin.com/company/coinchange/
- https://www.facebook.com/coinchangeio/
- https://www.youtube.com/channel/UCLqsOYvSkkpla96_DoSC_Qg
features: 
developerName: Coinchange Financials Inc

---

{% include copyFromAndroid.html %}

