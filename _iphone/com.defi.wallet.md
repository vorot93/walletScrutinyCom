---
wsId: cryptoComDefi
title: Crypto.com l DeFi Wallet
altTitle: 
authors:
- leo
appId: com.defi.wallet
appCountry: 
idd: 1512048310
released: 2020-05-20
updated: 2023-06-12
version: 1.60.0
stars: 4.6
reviews: 6126
size: '206727168'
website: https://crypto.com/defi-wallet
repository: 
issue: 
icon: com.defi.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-24
signer: 
reviewArchive: 
twitter: cryptocom
social:
- https://www.linkedin.com/company/cryptocom
- https://www.facebook.com/CryptoComOfficial
- https://www.reddit.com/r/Crypto_com
features: 
developerName: DeFi Labs

---

{% include copyFromAndroid.html %}
