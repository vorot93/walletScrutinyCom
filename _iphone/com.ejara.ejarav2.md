---
wsId: ejara
title: Ejara
altTitle: 
authors:
- danny
appId: com.ejara.ejarav2
appCountry: fr
idd: '1541127587'
released: 2021-03-22
updated: 2023-05-02
version: 3.4.1+183
stars: 4.1
reviews: 29
size: '127847424'
website: https://www.ejara.io
repository: 
issue: 
icon: com.ejara.ejarav2.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-02-21
signer: 
reviewArchive: 
twitter: EjaraApp
social:
- https://www.facebook.com/Ejaracapital
features: 
developerName: Ejara

---

{% include copyFromAndroid.html %}
