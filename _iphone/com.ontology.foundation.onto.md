---
wsId: ONTO
title: ONTO-Cross-chain Crypto Wallet
altTitle: 
authors:
- danny
appId: com.ontology.foundation.onto
appCountry: us
idd: 1436009823
released: 2018-09-21
updated: 2023-05-25
version: 4.5.1
stars: 4.1
reviews: 77
size: '259616768'
website: https://www.onto.app
repository: 
issue: 
icon: com.ontology.foundation.onto.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-09-15
signer: 
reviewArchive: 
twitter: ONTOWallet
social: 
features: 
developerName: Ontology Foundation

---

{% include copyFromAndroid.html %}