---
wsId: 
title: Stack Wallet by Cypher Stack
altTitle: 
authors: 
appId: com.cypherstack.stackwallet
appCountry: 
idd: '1634811534'
released: 2022-08-26
updated: 2023-06-09
version: 1.7.13
stars: 3.9
reviews: 11
size: '104591360'
website: https://stackwallet.com/
repository: 
issue: 
icon: com.cypherstack.stackwallet.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-03-23
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Cypher Stack LLC

---

