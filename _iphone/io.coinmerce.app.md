---
wsId: coinmerce
title: Coinmerce - Bitcoin Kopen
altTitle: 
authors:
- danny
appId: io.coinmerce.app
appCountry: nl
idd: '1409599830'
released: 2018-07-29
updated: 2023-06-09
version: 5.6.5
stars: 3.8
reviews: 4
size: '172316672'
website: https://coinmerce.io/en/
repository: 
issue: 
icon: io.coinmerce.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-15
signer: 
reviewArchive: 
twitter: coinmerce
social:
- https://www.facebook.com/CoinmerceNL
- https://www.linkedin.com/company/coinmerce/
features: 
developerName: Coinmerce BV

---

{% include copyFromAndroid.html %}

