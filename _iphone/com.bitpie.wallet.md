---
wsId: bitpie
title: Bitpie-Universal Crypto Wallet
altTitle: 
authors:
- leo
appId: com.bitpie.wallet
appCountry: 
idd: 1481314229
released: 2019-10-01
updated: 2023-06-01
version: 5.0.142
stars: 3.5
reviews: 153
size: '340749312'
website: https://bitpie.com
repository: 
issue: 
icon: com.bitpie.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: BitpieWallet
social:
- https://www.facebook.com/BitpieOfficial
- https://www.reddit.com/r/BitpieWallet
features:
- ln
developerName: BITPIE HK LIMITED

---

 {% include copyFromAndroid.html %}
