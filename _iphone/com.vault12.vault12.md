---
wsId: vault12
title: 'Vault12 Guard: Web3 Assets'
altTitle: 
authors:
- danny
appId: com.vault12.vault12
appCountry: us
idd: '1451596986'
released: 2019-08-28
updated: 2023-04-11
version: 2.0.1
stars: 4.7
reviews: 15
size: '43483136'
website: https://vault12.com
repository: https://github.com/vault12
issue: 
icon: com.vault12.vault12.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-03-23
signer: 
reviewArchive: 
twitter: _vault12_
social:
- https://www.facebook.com/GetVault12/
- https://www.linkedin.com/company/vault12/
- https://www.instagram.com/vault12/
- https://www.youtube.com/channel/UCoH4zPOpJhq6RbTZqUqzFwA
features: 
developerName: Vault12, Inc.

---

{% include copyFromAndroid.html %}
