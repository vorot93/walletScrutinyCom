---
wsId: huobi
title: Huobi - Buy Bitcoin & Crypto
altTitle: 
authors:
- leo
appId: com.huobi.appStoreHuobiSystem
appCountry: 
idd: 1023263342
released: 2015-08-19
updated: 2023-06-09
version: 10.1.0
stars: 4.1
reviews: 1356
size: '263533568'
website: https://www.huobi.com
repository: 
issue: 
icon: com.huobi.appStoreHuobiSystem.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive: 
twitter: HuobiGlobal
social:
- https://www.facebook.com/huobiglobalofficial
features: 
developerName: Huobi LTD

---

Neither on Google Play nor on their website can we find a claim of a
non-custodial part to this app. We assume it is a purely custodial interface to
the exchange of same name and therefore **not verifiable**.
