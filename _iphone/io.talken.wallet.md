---
wsId: Talken
title: Talken Web3 Wallet & NFT Suite
altTitle: 
authors:
- kiwilamb
appId: io.talken.wallet
appCountry: 
idd: 1459475831
released: 2019-09-25
updated: 2023-05-08
version: 1.01.17
stars: 5
reviews: 6
size: '140256256'
website: https://talken.io/
repository: 
issue: 
icon: io.talken.wallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-06-04
signer: 
reviewArchive: 
twitter: Talken_
social: 
features: 
developerName: Colligence Inc.

---

{% include copyFromAndroid.html %}

