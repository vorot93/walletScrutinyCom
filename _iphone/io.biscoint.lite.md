---
wsId: biscoint
title: Bitybank | Bitypreço
altTitle: 
authors:
- danny
appId: io.biscoint.lite
appCountry: br
idd: '1588152503'
released: 2022-02-09
updated: 2023-05-31
version: 2.2.9
stars: 4.9
reviews: 3083
size: '68677632'
website: https://bitybank.com.br/
repository: 
issue: 
icon: io.biscoint.lite.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-03-02
signer: 
reviewArchive: 
twitter: BityOficial
social:
- https://www.facebook.com/bitybankoficial
features: 
developerName: Biscoint

---

{% include copyFromAndroid.html %}

