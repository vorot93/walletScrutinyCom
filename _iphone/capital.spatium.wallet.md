---
wsId: spatium
title: Spatium MPC Crypto Wallet
altTitle: 
authors:
- danny
appId: capital.spatium.wallet
appCountry: us
idd: 1404844195
released: 2018-08-06
updated: 2023-06-08
version: 3.1.12
stars: 4.6
reviews: 27
size: '76314624'
website: https://spatium.net/
repository: 
issue: 
icon: capital.spatium.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-17
signer: 
reviewArchive: 
twitter: spatium_news
social:
- https://www.linkedin.com/company/spatium-capital
- https://www.facebook.com/spatiumnews
features: 
developerName: CaspianTechnologies

---

{% include copyFromAndroid.html %}
