---
wsId: coinCap
title: CoinCap
altTitle: 
authors:
- danny
appId: io.coinCap.coinCap
appCountry: us
idd: '1074052280'
released: 2016-01-25
updated: 2021-10-13
version: 3.1.33
stars: 4.7
reviews: 41547
size: '69145600'
website: http://coincap.io
repository: 
issue: 
icon: io.coinCap.coinCap.jpg
bugbounty: 
meta: stale
verdict: nowallet
date: 2023-04-03
signer: 
reviewArchive: 
twitter: CoinCap_io
social:
- https://www.facebook.com/watch/?v=365220420785166
features: 
developerName: ShapeShift AG

---

{% include copyFromAndroid.html %}
