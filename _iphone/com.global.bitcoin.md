---
wsId: bitcoinglobal
title: Bitcoin Global
altTitle: 
authors:
- danny
appId: com.global.bitcoin
appCountry: us
idd: 1536910503
released: 2020-10-26
updated: 2023-04-24
version: 2.12.0
stars: 4.9
reviews: 67
size: '27646976'
website: https://bitcoin.global/
repository: 
issue: 
icon: com.global.bitcoin.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: BitcoinGlobalEx
social:
- https://www.facebook.com/BitcoinGlobalEx
- https://www.reddit.com/r/BITCOIN_GLOBAL
features: 
developerName: Bitcoin Global

---

{% include copyFromAndroid.html %}
