---
wsId: coinspot
title: CoinSpot - Buy & Sell Bitcoin
altTitle: 
authors:
- danny
appId: com.coinspot.app
appCountry: au
idd: 1541949985
released: 2020-12-13
updated: 2023-04-27
version: 2.2.31
stars: 4.8
reviews: 16259
size: '46757888'
website: https://www.coinspot.com.au/
repository: 
issue: 
icon: com.coinspot.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive: 
twitter: coinspotau
social:
- https://www.facebook.com/coinspotau
features: 
developerName: CoinSpot

---

{% include copyFromAndroid.html %}
