---
wsId: kassio
title: Kassio
altTitle: 
authors:
- danny
appId: com.Kassio
appCountry: in
idd: '1610537547'
released: 2022-03-25
updated: 2023-04-01
version: 1.1.1
stars: 5
reviews: 12
size: '112041984'
website: https://kassio.com/contact
repository: 
issue: 
icon: com.Kassio.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-31
signer: 
reviewArchive: 
twitter: KassioNetwork
social:
- https://www.linkedin.com/company/kassionetwork/
- https://www.facebook.com/KassioNetwork
- https://www.instagram.com/kassionetwork/
- https://www.youtube.com/channel/UCWeoqvy5jc8TeNq5fOGoAoA
features: 
developerName: Digital Pung ApS

---

{% include copyFromAndroid.html %}

