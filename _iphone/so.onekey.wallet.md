---
wsId: onekeySo.new
title: 'OneKey: Blockchain DeFi Wallet'
altTitle: 
authors:
- danny
appId: so.onekey.wallet
appCountry: us
idd: '1609559473'
released: 2022-04-27
updated: 2023-06-12
version: 4.7.0
stars: 5
reviews: 2552
size: '106448896'
website: https://onekey.so?utm_source=app_store
repository: https://github.com/OneKeyHQ/app-monorepo
issue: 
icon: so.onekey.wallet.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-04-18
signer: 
reviewArchive: 
twitter: OneKeyHQ
social:
- https://discord.com/invite/nwUJaTzjzv
features: 
developerName: ONEKEY LIMITED

---

{% include copyFromAndroid.html %}