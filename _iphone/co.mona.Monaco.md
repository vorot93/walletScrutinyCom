---
wsId: mona
title: Crypto.com Buy BTC, ETH, Shib
altTitle: 
authors:
- leo
appId: co.mona.Monaco
appCountry: 
idd: 1262148500
released: 2017-08-31
updated: 2023-06-12
version: 3.162.1
stars: 4.5
reviews: 149620
size: '454850560'
website: https://crypto.com/
repository: 
issue: 
icon: co.mona.Monaco.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-24
signer: 
reviewArchive: 
twitter: cryptocom
social:
- https://www.linkedin.com/company/cryptocom
- https://www.facebook.com/CryptoComOfficial
- https://www.reddit.com/r/Crypto_com
features: 
developerName: Crypto.com

---

{% include copyFromAndroid.html %}
