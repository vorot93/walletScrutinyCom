---
wsId: BtcTurk
title: BtcTurk | Bitcoin Alım Satımı
altTitle: 
authors:
- danny
appId: com.mobillium.btcturk
appCountry: tr
idd: 1503482896
released: 2020-04-09
updated: 2023-05-22
version: 1.26.2
stars: 4.6
reviews: 25444
size: '224732160'
website: https://www.btcturk.com
repository: 
issue: 
icon: com.mobillium.btcturk.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive: 
twitter: btcturk
social:
- https://www.linkedin.com/company/btcturk
- https://www.facebook.com/btcturk
features: 
developerName: ELIPTIK YAZILIM VE TICARET ANONIM SIRKETI

---

{% include copyFromAndroid.html %}
