---
wsId: everestKinvo
title: 'Kinvo: Gestão de Investimentos'
altTitle: 
authors:
- danny
appId: com.everest.kinvo
appCountry: BR
idd: '1327335329'
released: 2018-01-03
updated: 2023-06-12
version: 3.19.1
stars: 4.6
reviews: 14294
size: '60784640'
website: http://www.kinvo.com.br
repository: 
issue: 
icon: com.everest.kinvo.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2022-06-23
signer: 
reviewArchive: 
twitter: kinvoapp
social:
- https://www.linkedin.com/company/kinvoapp
- https://www.facebook.com/appkinvo
- https://www.instagram.com/kinvoapp
- https://www.youtube.com/kinvoapp
features: 
developerName: EVEREST.TI

---

{% include copyFromAndroid.html %}
