---
wsId: Bitget
title: Bitget - Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.bitget.exchange.global
appCountry: ua
idd: 1442778704
released: 2018-11-29
updated: 2023-06-12
version: 2.5.1
stars: 4
reviews: 197
size: '198118400'
website: https://www.bitget.com/en
repository: 
issue: 
icon: com.bitget.exchange.global.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-17
signer: 
reviewArchive: 
twitter: bitgetglobal
social:
- https://www.linkedin.com/company/bitget
- https://www.facebook.com/BitgetGlobal
features: 
developerName: SINGAPORE BITGET PTE.LTD

---

 {% include copyFromAndroid.html %}
