---
wsId: ELLIPAL
title: ELLIPAL
altTitle: 
authors:
- leo
appId: com.Ellipal.Ellipal
appCountry: us
idd: 1426179665
released: 2018-08-25
updated: 2023-06-03
version: 3.10.0
stars: 4.8
reviews: 2250
size: '70811648'
website: http://www.ellipal.com/
repository: 
issue: 
icon: com.Ellipal.Ellipal.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-12-03
signer: 
reviewArchive: 
twitter: ellipalwallet
social:
- https://www.facebook.com/ellipalclub
- https://www.reddit.com/r/ELLIPAL_Official
features: 
developerName: Ellipal

---

{% include copyFromAndroid.html %}