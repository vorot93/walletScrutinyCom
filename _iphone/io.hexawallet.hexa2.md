---
wsId: Hexa2
title: Bitcoin Tribe
altTitle: 
authors:
- danny
appId: io.hexawallet.hexa2
appCountry: in
idd: 1586334138
released: 2021-11-01
updated: 2023-05-15
version: 2.1.7
stars: 5
reviews: 8
size: '50296832'
website: https://hexawallet.io/
repository: https://github.com/bithyve/hexa
issue: 
icon: io.hexawallet.hexa2.jpg
bugbounty: 
meta: ok
verdict: ftbfs
date: 2021-12-19
signer: 
reviewArchive: 
twitter: HexaWallet
social:
- https://www.linkedin.com/company/bithyve
features: 
developerName: Bithyve

---

{% include copyFromAndroid.html %}