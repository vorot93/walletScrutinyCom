---
wsId: xverse
title: Xverse - Bitcoin Web3 Wallet
altTitle: 
authors:
- danny
appId: com.secretkeylabs.xverse
appCountry: gt
idd: 1552272513
released: 2021-10-15
updated: 2023-06-02
version: v1.17.1
stars: 5
reviews: 1
size: '39537664'
website: https://twitter.com/xverseApp
repository: 
issue: 
icon: com.secretkeylabs.xverse.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-13
signer: 
reviewArchive: 
twitter: secretkeylabs
social: 
features: 
developerName: Secret Key Labs

---

{% include copyFromAndroid.html %}

