---
wsId: niceHash
title: NiceHash
altTitle: 
authors:
- danny
appId: com.nicehash.mobile
appCountry: us
idd: '1372054956'
released: 2020-04-29
updated: 2023-06-08
version: 5.4.2
stars: 4.4
reviews: 1902
size: '96885760'
website: https://www.nicehash.com/
repository: 
issue: 
icon: com.nicehash.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-06-24
signer: 
reviewArchive: 
twitter: NiceHashMining
social:
- https://www.linkedin.com/company/nicehash
- https://www.instagram.com/nicehash_official
- https://www.reddit.com/r/NiceHash
- https://www.facebook.com/NiceHash
features: 
developerName: H-BIT, d.o.o.

---

{% include copyFromAndroid.html %}
