---
wsId: immuneMessenger
title: IMMUNE Messenger
altTitle: 
authors:
- danny
appId: im.immune.app
appCountry: ru
idd: '1489335158'
released: 2019-12-09
updated: 2023-03-27
version: 2.0.33
stars: 5
reviews: 4
size: '86696960'
website: https://imm.app/
repository: 
issue: 
icon: im.immune.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-04-15
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: IMM Money Messenger Limited

---

{% include copyFromAndroid.html %}
