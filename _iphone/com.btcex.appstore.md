---
wsId: BTCEX
title: 'BTCEX: Buy BTC, Cryptocurrency'
altTitle: 
authors:
- danny
appId: com.btcex.appstore
appCountry: am
idd: '1580680668'
released: 2021-10-22
updated: 2023-05-21
version: 1.4.2
stars: 0
reviews: 0
size: '99433472'
website: https://www.btcex.com/
repository: 
issue: 
icon: com.btcex.appstore.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-07-18
signer: 
reviewArchive: 
twitter: BTCEX_exchange
social:
- https://www.reddit.com/user/BTCEX_exchange
- https://www.facebook.com/BTCEX.exchange
- https://www.linkedin.com/company/btcex/about/
- https://blog.naver.com/btcex
- https://medium.com/@BTCEX
features: 
developerName: BTCEX EXCHANGE LIMITED

---

{% include copyFromAndroid.html %}