---
wsId: getcoinsWallet
title: GetCoins Wallet
altTitle: 
authors:
- danny
appId: com.getcoins.gcwallet
appCountry: us
idd: '1426011288'
released: 2018-08-18
updated: 2023-04-26
version: 15.2.0
stars: 2.5
reviews: 13
size: '61375488'
website: https://getcoins.com/
repository: 
issue: 
icon: com.getcoins.gcwallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-05-25
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Evergreen ATM LLC

---

{% include copyFromAndroid.html %}