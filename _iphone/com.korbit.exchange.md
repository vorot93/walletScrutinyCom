---
wsId: korbit
title: 코빗 - 대한민국 최초, 가상자산 거래소
altTitle: 
authors:
- danny
appId: com.korbit.exchange
appCountry: us
idd: 1434511619
released: 2018-10-18
updated: 2023-04-25
version: 7.4.2
stars: 2.7
reviews: 15
size: '189559808'
website: http://www.korbit.co.kr
repository: 
issue: 
icon: com.korbit.exchange.jpg
bugbounty: 
meta: defunct
verdict: custodial
date: 2023-05-30
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Korbit

---

{% include copyFromAndroid.html %}
