---
wsId: AirGapVault
title: AirGap Vault - Secure Secrets
altTitle: 
authors:
- leo
appId: it.airgap.vault
appCountry: 
idd: 1417126841
released: 2018-08-24
updated: 2023-05-23
version: 3.26.0
stars: 4.5
reviews: 16
size: '91831296'
website: 
repository: https://github.com/airgap-it/airgap-vault
issue: 
icon: it.airgap.vault.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2021-09-29
signer: 
reviewArchive: 
twitter: AirGap_it
social:
- https://www.reddit.com/r/AirGap
features: 
developerName: Papers GmbH

---

This app for Android is reproducible but unfortunately due to limitations of the
iPhone platform, we so far were not able to reproduce any App Store app.
