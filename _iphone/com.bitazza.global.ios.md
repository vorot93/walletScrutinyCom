---
wsId: bitazzaGL
title: 'Bitazza: Crypto Exchange'
altTitle: 
authors:
- danny
appId: com.bitazza.global.ios
appCountry: th
idd: '1612226119'
released: 2022-04-07
updated: 2022-12-28
version: 2.8.16
stars: 4
reviews: 24
size: '159729664'
website: https://www.bitazza.com
repository: 
issue: 
icon: com.bitazza.global.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-09
signer: 
reviewArchive: 
twitter: BitazzaGlobal
social:
- https://www.facebook.com/bitazzaglobal
- https://www.linkedin.com/company/bitazza/
- https://t.me/bitazzaglobal
features: 
developerName: Bitazza Company Limited

---

{% include walletLink.html wallet='android/' verdict='true' %}

