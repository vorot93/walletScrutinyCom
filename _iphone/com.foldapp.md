---
wsId: foldapp
title: 'Fold: Earn and buy bitcoin'
altTitle: 
authors:
- danny
appId: com.foldapp
appCountry: us
idd: 1480424785
released: 2019-11-18
updated: 2023-05-11
version: 139.22.0
stars: 4.4
reviews: 1528
size: '42502144'
website: http://foldapp.com
repository: 
issue: 
icon: com.foldapp.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-09-15
signer: 
reviewArchive: 
twitter: fold_app
social: 
features: 
developerName: Fold

---

{% include copyFromAndroid.html %}
