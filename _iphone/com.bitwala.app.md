---
wsId: Nuri
title: 'Bitwala: Bitcoin Wallet'
altTitle: 
authors:
- danny
appId: com.bitwala.app
appCountry: gd
idd: 1454003161
released: 2019-05-11
updated: 2023-05-10
version: 2.5.1
stars: 0
reviews: 0
size: '101055488'
website: https://www.bitwala.com
repository: 
issue: 
icon: com.bitwala.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-12-22
signer: 
reviewArchive: 
twitter: nuribanking
social: 
features: 
developerName: Bitwala

---

{% include copyFromAndroid.html %}