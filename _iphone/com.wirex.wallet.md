---
wsId: wirexwalletdefi
title: 'Wirex Wallet: Crypto and DeFi'
altTitle: 
authors:
- danny
appId: com.wirex.wallet
appCountry: nz
idd: 1594165139
released: 2021-12-07
updated: 2023-04-20
version: 0.3.15
stars: 4.3
reviews: 7
size: '78412800'
website: https://wirexapp.com/wirex-wallet
repository: 
issue: 
icon: com.wirex.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-10
signer: 
reviewArchive: 
twitter: wirexapp
social:
- https://github.com/wirexapp
features: 
developerName: Wirex Limited

---

{% include copyFromAndroid.html %}
