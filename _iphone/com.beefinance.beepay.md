---
wsId: beFiWeb3
title: BeFi
altTitle: 
authors:
- danny
appId: com.beefinance.beepay
appCountry: sg
idd: '1592439709'
released: 2021-11-10
updated: 2023-06-11
version: 2.1.9
stars: 0
reviews: 0
size: '93088768'
website: https://www.befiwallet.io
repository: 
issue: 
icon: com.beefinance.beepay.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-03-31
signer: 
reviewArchive: 
twitter: BeFiWalletverse
social:
- https://t.me/befiwallet
features: 
developerName: BEFI FOUNDATION LTD.

---

{% include copyFromAndroid.html %}