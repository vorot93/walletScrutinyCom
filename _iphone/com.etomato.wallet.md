---
wsId: etomato
title: 통통지갑
altTitle: 
authors:
- danny
appId: com.etomato.wallet
appCountry: kr
idd: '1618695778'
released: 2022-04-24
updated: 2023-05-03
version: 1.3.2
stars: 4
reviews: 2
size: '54343680'
website: https://tongtongwallet.com/
repository: 
issue: 
icon: com.etomato.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-04-12
signer: 
reviewArchive: 
twitter: tomatochain
social:
- https://www.instagram.com/tomatochain_official/
- https://t.me/tomatochain_official_Eng
features: 
developerName: eTomato

---

{% include copyFromAndroid.html %}

