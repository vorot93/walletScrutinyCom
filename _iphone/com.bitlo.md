---
wsId: bitlo
title: 'Bitlo: Bitcoin & Kripto Para'
altTitle: 
authors:
- danny
appId: com.bitlo
appCountry: tr
idd: '1544492069'
released: 2020-12-28
updated: 2023-04-29
version: 2.1.0
stars: 3.6
reviews: 322
size: '48929792'
website: https://www.bitlo.com/
repository: 
issue: 
icon: com.bitlo.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-02-08
signer: 
reviewArchive: 
twitter: bitlocom
social:
- https://www.linkedin.com/company/bitlo/
features: 
developerName: Bitlo Teknoloji Anonim Şirketi

---

{% include copyFromAndroid.html %}

