---
wsId: kointra
title: Kointra
altTitle: 
authors:
- danny
appId: com.kointra.Kointra.V1.0
appCountry: tr
idd: '1486286836'
released: 2019-11-15
updated: 2021-09-15
version: 3.1.2
stars: 4.7
reviews: 30
size: '69696512'
website: https://kointra.com
repository: 
issue: 
icon: com.kointra.Kointra.V1.0.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-04-28
signer: 
reviewArchive: 
twitter: KointraTR
social:
- https://www.instagram.com/kointracom/
features: 
developerName: Kointra A.S

---

{% include copyFromAndroid.html %}