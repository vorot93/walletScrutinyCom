---
wsId: changeinvest
title: 'Change: Invest & Trade'
altTitle: 
authors:
- danny
appId: com.getchange.dev
appCountry: nl
idd: 1442085358
released: 2018-11-15
updated: 2023-06-12
version: 30.43.1
stars: 4.1
reviews: 33
size: '117925888'
website: https://www.changeinvest.com/
repository: 
issue: 
icon: com.getchange.dev.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-01-04
signer: 
reviewArchive: 
twitter: changefinance
social:
- https://www.linkedin.com/company/changeinvest
- https://www.facebook.com/changeinvest
features: 
developerName: xChange AS

---

{% include copyFromAndroid.html %}
