---
wsId: 
title: Skrill - Pay & Send Money
altTitle: 
authors: 
appId: com.skrill.ios.SkrillPayments
appCountry: gb
idd: '718248239'
released: 2013-10-29
updated: 2023-06-02
version: 3.112.0
stars: 4.5
reviews: 5319
size: '224516096'
website: https://www.skrill.com/
repository: 
issue: 
icon: com.skrill.ios.SkrillPayments.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-06-09
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Skrill Ltd.

---

