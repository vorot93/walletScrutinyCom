---
wsId: crypterium
title: 'Сhoise.com: Crypto Wallet. NFT'
altTitle: 
authors:
- leo
appId: com.Crypterium.Crypterium
appCountry: 
idd: 1360632912
released: 2018-03-26
updated: 2023-04-10
version: '3.3'
stars: 4.2
reviews: 920
size: '244340736'
website: https://cards.crypterium.com/visa
repository: 
issue: 
icon: com.Crypterium.Crypterium.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-20
signer: 
reviewArchive: 
twitter: crypterium
social:
- https://www.facebook.com/crypterium.org
features: 
developerName: Crypterium AS

---

Judging by what we can find on the [wallet site](https://wallet.crypterium.com/):

> **Store**<br>
  keep your currencies<br>
  safe & fully insured

this is a custodial app as a self-custody wallet cannot ever have funds insured.

As a custodial app it is **not verifiable**.
