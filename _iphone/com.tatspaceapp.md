---
wsId: tatspace
title: TATSPACE
altTitle: 
authors:
- danny
appId: com.tatspaceapp
appCountry: us
idd: '1629762278'
released: 2022-06-21
updated: 2023-04-11
version: 1.0.15
stars: 3.9
reviews: 40
size: '86139904'
website: https://www.tatcoin.com
repository: 
issue: 
icon: com.tatspaceapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-12
signer: 
reviewArchive: 
twitter: tatspaceapp
social:
- https://www.linkedin.com/company/the-abit-app/
- https://t.me/tatspaceapp
- https://www.instagram.com/tatspaceapp/
features: 
developerName: TATSPACE

---

{% include copyFromAndroid.html %}
