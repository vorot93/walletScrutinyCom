---
wsId: Skilling
title: 'Skilling: Invest Forex & Trade'
altTitle: 
authors:
- danny
appId: com.FinovelCyprusLimited.Skilling
appCountry: gb
idd: 1441386723
released: 2019-05-30
updated: 2023-05-05
version: 2.0.2
stars: 4.9
reviews: 12
size: '17238016'
website: https://skilling.com
repository: 
issue: 
icon: com.FinovelCyprusLimited.Skilling.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-10-10
signer: 
reviewArchive: 
twitter: SkillingTrading
social:
- https://www.linkedin.com/company/skilling
- https://www.facebook.com/SkillingTrading
features: 
developerName: Skilling Limited

---

{% include copyFromAndroid.html %}
