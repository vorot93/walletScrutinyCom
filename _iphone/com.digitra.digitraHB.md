---
wsId: digitraTrading
title: Digitra.com - Zero fee trading
altTitle: 
authors:
- danny
appId: com.digitra.digitraHB
appCountry: us
idd: '1577787451'
released: 2022-03-12
updated: 2023-05-09
version: 1.0.50
stars: 0
reviews: 0
size: '92386304'
website: https://www.digitra.com
repository: 
issue: 
icon: com.digitra.digitraHB.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-31
signer: 
reviewArchive: 
twitter: Digitra_com
social:
- https://www.facebook.com/DigitraGlobal/
- https://www.linkedin.com/company/digitra-global/
- https://www.instagram.com/digitracom/
- https://www.youtube.com/@digitracom
features: 
developerName: 

---

{% include copyFromAndroid.html %}