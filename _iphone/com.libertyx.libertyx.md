---
wsId: libertyx
title: LibertyX - Buy Bitcoin
altTitle: 
authors:
- danny
appId: com.libertyx.libertyx
appCountry: us
idd: 966538981
released: 2015-02-20
updated: 2021-06-15
version: 4.1.0
stars: 3.9
reviews: 162
size: '14478336'
website: https://libertyx.com
repository: 
issue: 
icon: com.libertyx.libertyx.jpg
bugbounty: 
meta: obsolete
verdict: nowallet
date: 2023-06-06
signer: 
reviewArchive: 
twitter: libertyx
social:
- https://www.linkedin.com/company/libertyx
- https://www.facebook.com/getlibertyx
features: 
developerName: Moon, Inc.

---

 {% include copyFromAndroid.html %}
