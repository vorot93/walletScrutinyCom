---
wsId: ankerPay
title: 'AnkerPay Wallet : Bitcoin'
altTitle: 
authors:
- danny
appId: com.ankerpay.ioswallet
appCountry: us
idd: 1487931971
released: 2019-11-22
updated: 2023-06-06
version: '1.26'
stars: 0
reviews: 0
size: '56749056'
website: https://ankerpay.com/mobile-wallet/
repository: 
issue: 
icon: com.ankerpay.ioswallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-06-08
signer: 
reviewArchive: 
twitter: AnkerPay
social:
- https://www.facebook.com/AnkerPlatform
features: 
developerName: AnkerPay

---

{% include copyFromAndroid.html %}