---
wsId: prestmit
title: 'Prestmit: Gift Cards & Coins'
altTitle: 
authors:
- danny
appId: com.prestmit.app
appCountry: us
idd: 1581960714
released: 2021-08-20
updated: 2023-06-04
version: 6.0.2
stars: 4.1
reviews: 1045
size: '98194432'
website: https://prestmit.com
repository: 
issue: 
icon: com.prestmit.app.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-11-02
signer: 
reviewArchive: 
twitter: prestmit
social:
- https://www.facebook.com/prestmit
features: 
developerName: Prestmit Technologies LTD.

---

{% include copyFromAndroid.html %}
