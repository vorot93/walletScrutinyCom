---
wsId: tapngo
title: Tap - Buy & Sell Bitcoin
altTitle: 
authors:
- danny
appId: com.TapGlobal.tap
appCountry: gb
idd: 1492263993
released: 2019-12-20
updated: 2023-06-12
version: 2.7.5
stars: 4.7
reviews: 1344
size: '195425280'
website: https://www.withtap.com
repository: 
issue: 
icon: com.TapGlobal.tap.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Tap Global Limited

---

 {% include copyFromAndroid.html %}
