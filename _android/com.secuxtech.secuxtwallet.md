---
wsId: 
title: SecuX Wallet
altTitle: 
authors:
- danny
users: 500
appId: com.secuxtech.secuxtwallet
appCountry: TW
released: 2022-05-20
updated: 2023-06-05
version: 1.3.04
stars: 
ratings: 
reviews: 
size: 
website: https://secuxtech.com/
repository: 
issue: 
icon: com.secuxtech.secuxtwallet.png
bugbounty: 
meta: ok
verdict: fewusers
date: 2022-11-24
signer: 
reviewArchive: 
twitter: SecuXwallet
social: 
redirect_from: 
developerName: SecuX
features: 

---

This is the companion app to the {% include walletLink.html wallet='hardware/secuxnifty' verdict='true' %}