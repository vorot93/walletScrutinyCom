---
wsId: paybankExchange
title: '24Paybank: Купить и продать Би'
altTitle: 
authors:
- danny
users: 5000
appId: com.exchanger24pb.myapplication24
appCountry: 
released: 2020-02-07
updated: 2020-02-27
version: '1.2'
stars: 
ratings: 
reviews: 
size: 
website: https://24paybank.net/
repository: 
issue: 
icon: com.exchanger24pb.myapplication24.png
bugbounty: 
meta: obsolete
verdict: nowallet
date: 2023-04-22
signer: 
reviewArchive: 
twitter: 24Paybank
social: 
redirect_from: 
developerName: Dmitry24pb
features: 

---

## App Description from Google Play 

> Buy or sell Bitcoin for rubles, as well as other currencies, using bank cards or popular electronic payment systems

## Analysis 

- The app merely shows the website in a mobile format 
- It is similar to Shapeshift.io: 
  - the user inputs the amount to be exchange, the processor, and the receiving address.

This app **does not have a wallet**. 