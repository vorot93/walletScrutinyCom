---
wsId: traderWagon
title: TraderWagon
altTitle: 
authors:
- danny
users: 10000
appId: com.traderwagon.android
appCountry: 
released: 2022-05-24
updated: 2023-06-02
version: 1.7.1
stars: 4.5
ratings: 
reviews: 12
size: 
website: https://www.traderwagon.com/
repository: 
issue: 
icon: com.traderwagon.android.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-04-12
signer: 
reviewArchive: 
twitter: traderwagon
social: 
redirect_from: 
developerName: TraderWagon Ltd.
features: 

---

## App Description from Google Play 

> TraderWagon uses Binance’s Single Sign-On API, allowing traders to use their Binance account log-in to conveniently and securely create their TraderWagon account and manage their copy trading portfolio.

It does **not have its own wallet functionality**. 

