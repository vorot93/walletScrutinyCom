---
wsId: payBoltBusiness
title: PayBolt Business
altTitle: 
authors:
- danny
users: 1000
appId: com.fincrypt.payboltmerchant
appCountry: 
released: 2022-03-18
updated: 2022-10-25
version: 3.0.2
stars: 4.8
ratings: 
reviews: 5
size: 
website: https://paybolt.com
repository: 
issue: 
icon: com.fincrypt.payboltmerchant.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-05-25
signer: 
reviewArchive: 
twitter: PayBoltOfficial
social:
- https://www.instagram.com/paybolt
- https://www.facebook.com/PayBolt
- https://www.linkedin.com/company/payboltofficial
- https://t.me/PayBolt
redirect_from: 
developerName: FinCrypt
features: 

---

## App Description from Google Play

> PayBolt Business integrates seamlessly to your business, allowing you to receive any cryptocurrency payments from worldwide customers instantly.

## Analysis 

- [Screenshots](https://twitter.com/BitcoinWalletz/status/1661558994903658496)
- The app only supports ETH ERC20, BSC BEP20, Polygon Matic. 
- There is **no support for BTC**.