---
wsId: 
title: Omni Web3 Wallet
altTitle: 
authors:
- danny
users: 10000
appId: fi.steakwallet.app
appCountry: 
released: 2021-07-07
updated: 2023-05-25
version: 3.0.42
stars: 4.2
ratings: 
reviews: 22
size: 
website: https://steakwallet.fi
repository: 
issue: 
icon: fi.steakwallet.app.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-05-03
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Steakwallet
features: 

---

## App Description from Google Play 

> Multi-Chain Support: Forget using a different wallet for every chain. Omni supports more than 25 blockchains with no setup required
> 
> Your Keys, Your Crypto: You are always in full control of your wallet and your crypto.

## Analysis 

All bitcoin wallets on the app are not on the Bitcoin blockchain, but tokenized representations on Solana and other blockchains. 

This app does not have a Bitcoin wallet. 
