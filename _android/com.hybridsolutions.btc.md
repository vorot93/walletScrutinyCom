---
wsId: 
title: BTC VertexFX Trader
altTitle: 
authors:
- danny
users: 10000
appId: com.hybridsolutions.btc
appCountry: 
released: 2020-08-24
updated: 2022-04-21
version: '1.3'
stars: 4.6
ratings: 
reviews: 1
size: 
website: https://hybridsolutions.com/
repository: 
issue: 
icon: com.hybridsolutions.btc.png
bugbounty: 
meta: stale
verdict: nowallet
date: 2023-04-16
signer: 
reviewArchive: 
twitter: VertexFXTrader
social:
- https://www.linkedin.com/company/hybrid-solutions-hs-
- https://www.facebook.com/VertexFXTrader
redirect_from: 
developerName: Hybrid Solutions - VertexFX Trader
features: 

---

## App Description

> VertexFX Android mobile trader is used to trade Forex, Stock markets, bullion and commodities physically with ease.

Although its name is "BTC VertexFX Trader", the BTC stands for "Bullion Trading Company" and not the ticker symbol for Bitcoin. It does not mention "Bitcoin" "Cryptocurrencies" or "crypto" in its app description page or on its homepage.

Its [Terms](https://hybridsolutions.com/Terms.pdf) also do not mention any cryptocurrency-related language.

It does mention CFDs, but not specifically for Bitcoin in its [EULA](https://hybridsolutions.com/EULA.pdf)

The way to deposit and/or withdraw is via [Paypal](https://support.hybridsolutions.com/Knowledgebase/Article/View/5755/612/mywallet)

## Verdict

This app does not have a wallet.

