---
wsId: byteWallet
title: ByteWallet
altTitle: 
authors:
- danny
users: 5000
appId: io.bytewallet.bytewallet
appCountry: 
released: 2021-07-20
updated: 2023-04-19
version: VARY
stars: 3
ratings: 
reviews: 32
size: 
website: https://www.bytefederal.com
repository: 
issue: 
icon: io.bytewallet.bytewallet.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-05-05
signer: 
reviewArchive: 
twitter: bytefederal
social:
- https://www.facebook.com/bytefederal
- https://www.instagram.com/bytefederalatm
- https://www.youtube.com/channel/UCozOzfZ0MgqLT_TA7hbNh4g
- https://www.linkedin.com/company/bytefederal
redirect_from: 
developerName: Byte Federal, Inc
features: 

---

## App Description from Google Play 

> Store, send, and receive Bitcoin safely and easily. Manage your ByteFederal account and find our nearest Bitcoin ATM locations.

## Analysis 

[(Screenshots)](https://twitter.com/BitcoinWalletz/status/1654313718086529025)
- We installed the app and was able to create a Bitcoin wallet.
- The wallet can send/receive.
- The app provided the backup phrase.
- There are no claims the app is Open Source.
- We were not able to find the corresponding app ID on GitHub.
- This app's source is **not publicly available.**
