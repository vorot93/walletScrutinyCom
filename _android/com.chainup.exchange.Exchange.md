---
wsId: scallopExchange
title: Scallop Exchange
altTitle: 
authors:
- danny
users: 10000
appId: com.chainup.exchange.Exchange
appCountry: 
released: 2022-06-21
updated: 2022-09-30
version: 5.4.12
stars: 4
ratings: 
reviews: 8
size: 
website: http://www.scallop.exchange
repository: 
issue: 
icon: com.chainup.exchange.Exchange.png
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-30
signer: 
reviewArchive: 
twitter: ScallopOfficial
social:
- https://www.linkedin.com/company/scallopx/
redirect_from: 
developerName: Scallop
features: 

---

## App Description 

> The Scallop Exchange is the fulcrum of our payment system and will facilitate the conversion of cryptocurrency / DeFi tokens into fiat and vice versa. The Exchange supports both hot and cold wallets, providing users with ample choices to match their desired level of convenience and security. 

## Analysis 

> From the FAQ: What makes Scallop different? 
>
> What sets us apart from the likes of Crypto.com is that we offer a range of additional banking services. These include:
>
> - UK Bank Account with sort code 
> - UE bank accounts with IBAN 
> - Direct debit cards
> - Cold Crypto Wallets 
> - Dfi token acceptance 
>
> When making payments, existing crypto payment cards convert cryptocurrency into cash to pay merchants. Our card is different, because we are a banking app that offers crypto services, you have the ability to open a bank account that will be directly integrated with your cryptocurrency wallet. When a purchase is made, scallop converts crypto into cash to pay our merchants instantly in their business accounts upon completion of the transaction.

We installed the app and found a BTC deposit and withdrawal option. We were not able to find options to backup seed phrases. 

## Verdict 

The exchange supports both hot and cold wallets, and with no evidence that it supports the backup of seed phrases, this is highly likely to be a **custodial** service.

