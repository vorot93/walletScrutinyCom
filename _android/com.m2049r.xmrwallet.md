---
wsId: 
title: Monerujo - Monero Wallet
altTitle: 
authors:
- leo
users: 100000
appId: com.m2049r.xmrwallet
appCountry: 
released: 2017-09-29
updated: 2023-05-29
version: 3.2.0 'Decoy Selection'
stars: 3.3
ratings: 821
reviews: 169
size: 
website: https://monerujo.io/
repository: 
issue: 
icon: com.m2049r.xmrwallet.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-02-27
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: m2049r
features: 

---

This app does not feature BTC wallet functionality.