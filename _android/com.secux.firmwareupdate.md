---
wsId: 
title: SecuX Firmware Update
altTitle: 
authors:
- danny
users: 5000
appId: com.secux.firmwareupdate
appCountry: 
released: 2021-09-28
updated: 2022-07-04
version: 1.0.5
stars: 2.6
ratings: 
reviews: 8
size: 
website: https://www.secuxtech.com
repository: 
issue: 
icon: com.secux.firmwareupdate.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-06-01
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: SecuX
features: 

---

## App Description from Google Play 
>
> SecuX Firmware Update App provides a fast, safe and easy firmware update process via Bluetooth connection for SecuX V20, W20 and Nifty hardware wallets. The interactive step-by-step guide prepares users to follow instructions, and complete firmware update in just a few minutes.
>
> Before You Start
>
> Please make sure you have all of the following items: 
> - Recovery Words & Passphrase
- A Safe and Stable Internet Connection
- iOS device and Wallet sufficiently charged and a charger. 
>
> SecuX Firmware Update App is fully compatible with SecuX V20, W20 and Nifty hardware wallets via Bluetooth connection.

## Analysis 

As the name implies the app only exists to update the firmware of SecuX devices. 

It is compatible with: 

- {% include walletLink.html wallet='hardware/secuxstonev20' verdict='true' %}
- {% include walletLink.html wallet='hardware/secuxnifty' verdict='true' %}
- {% include walletLink.html wallet='hardware/secuxstonew20' verdict='true' %}

This is **not a wallet**.
