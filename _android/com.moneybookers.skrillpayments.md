---
wsId: 
title: Skrill - Fast, secure payments
altTitle: 
authors:
- danny
users: 10000000
appId: com.moneybookers.skrillpayments
appCountry: 
released: 2013-10-29
updated: 2023-06-01
version: 3.112.0-2023053011
stars: 4.4
ratings: 142059
reviews: 2878
size: 
website: https://www.skrill.com
repository: 
issue: 
icon: com.moneybookers.skrillpayments.png
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-08-09
signer: 
reviewArchive: 
twitter: skrill
social:
- https://www.facebook.com/Skrillpayments
- https://www.instagram.com/skrill
redirect_from: 
developerName: Paysafe
features: 

---

> The app empowers you to make online payments, send money to a friend or relative, and exchange cryptocurrencies.

From their terms of use:

> 5.2.2 you will not have a personal cryptocurrency wallet with the Cryptocurrency Exchange

> The Supported Cryptocurrencies (and the fiat currency used to buy/sell them) are held in custody by a third party.

Based on this information, the app is custodial therefore not verifiable.
