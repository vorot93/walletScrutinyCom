---
wsId: 
title: Rubix Wallet
altTitle: 
authors: 
users: 100
appId: io.rubix.exchange
appCountry: 
released: 2021-12-22
updated: 2023-02-14
version: 1.0.8
stars: 
ratings: 
reviews: 
size: 
website: https://rubix.io
repository: 
issue: 
icon: io.rubix.exchange.jpg
bugbounty: 
meta: ok
verdict: fewusers
date: 2023-06-02
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: 'Rubix: Buy, Sell, and Trade Crypto'
features: 

---

