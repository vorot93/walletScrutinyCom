---
wsId: krakent
title: 'Kraken Pro: Crypto Trading'
altTitle: 
authors:
- leo
users: 1000000
appId: com.kraken.trade
appCountry: 
released: 2019-10-24
updated: 2023-05-17
version: 2.35.0
stars: 4.5
ratings: 25287
reviews: 3032
size: 
website: https://www.kraken.com
repository: 
issue: 
icon: com.kraken.trade.png
bugbounty: 
meta: ok
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive: 
twitter: krakenfx
social:
- https://www.linkedin.com/company/krakenfx
- https://www.facebook.com/KrakenFX
redirect_from:
- /com.kraken.trade/
- /posts/com.kraken.trade/
developerName: Payward, Inc.
features:
- ln

---

On their website we read:

> 95% of all deposits are kept in offline, air-gapped, geographically
  distributed cold storage. We keep full reserves so that you can always
  withdraw immediately on demand.

This app is an interface to a custodial exchange and therefore **not
verifiable**.
