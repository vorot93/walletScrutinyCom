---
wsId: 
title: Bitgert Exchange
altTitle: 
authors: 
users: 10000
appId: exchange.bitgert.app
appCountry: 
released: 2023-01-13
updated: 2023-01-13
version: '1.0'
stars: 4.3
ratings: 
reviews: 28
size: 
website: https://bitgert.exchange
repository: 
issue: 
icon: exchange.bitgert.app.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-06-03
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Bitgert Technologies Ltd
features: 

---

