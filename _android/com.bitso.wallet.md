---
wsId: bitso
title: Bitso
altTitle: 
authors:
- leo
users: 5000000
appId: com.bitso.wallet
appCountry: 
released: 2018-02-19
updated: 2023-06-12
version: 3.37.6
stars: 4.3
ratings: 40512
reviews: 271
size: 
website: https://bitso.com/app
repository: 
issue: 
icon: com.bitso.wallet.png
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive: 
twitter: Bitso
social:
- https://www.facebook.com/bitsoex
redirect_from:
- /com.bitso.wallet/
developerName: Bitso
features: 

---

Bitso appears to be an exchange and their statement on security on their website

> **Maximum security**<br>
  We work every day to keep your account protected. That's why more than 2
  million users trust us.

is saying "trust us". Their security is **not verifiable**.
