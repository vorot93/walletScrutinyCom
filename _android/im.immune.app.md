---
wsId: immuneMessenger
title: IMMUNE Messenger
altTitle: 
authors:
- danny
users: 10000
appId: im.immune.app
appCountry: 
released: 2019-11-29
updated: 2023-04-26
version: 2.0.33
stars: 
ratings: 
reviews: 
size: 
website: https://imm.app/
repository: 
issue: 
icon: im.immune.app.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-04-15
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: IMM Money Messenger Limited
features: 

---

## App Description from [Google Play](https://play.google.com/store/apps/details?id=im.immune.app&gl=us) 

> IMMUNE contains all the usual messenger functions:
- Sending text and voice messages
- Voice and video calls
- Sending and receiving files of any type
- Sending and receiving images and videos
- Sending and receiving contacts from the address book
>
> The IMMUNE crypto wallet allows you to:
- Store bitcoins on the mobile device
- Transfer bitcoins to other people by phone number or a wallet address
- Buy bitcoins with credit card payment (not available in all countries)
- Restore the wallet using a secret phrase when reinstalling the app or changing the phone number

## Analysis 

The app has a Bitcoin wallet that can send and receive. It also provides the seed phrases. However, we were **not able to find a publicly available source** for the Android app. Thus, this app **cannot be verified**. 
