---
wsId: ChangeNOW
title: Crypto Exchange & Buy Crypto
altTitle: 
authors:
- leo
users: 100000
appId: io.changenow.changenow
appCountry: 
released: 2018-09-07
updated: 2023-06-08
version: 1.151.1
stars: 4.7
ratings: 2199
reviews: 523
size: 
website: https://changenow.io
repository: 
issue: 
icon: io.changenow.changenow.png
bugbounty: 
meta: ok
verdict: nosource
date: 2020-11-16
signer: 
reviewArchive: 
twitter: ChangeNOW_io
social:
- https://www.facebook.com/ChangeNOW.io
- https://www.reddit.com/r/ChangeNOW_io
redirect_from:
- /io.changenow.changenow/
developerName: ChangeNOW
features: 

---

> We focus on simplicity and safety — the service is registration-free and non-custodial.

> With ChangeNOW, you remain in full control over your digital assets.

That's a claim. Let's see if it is verifiable ...

There is no claim of public source anywhere and
[neither does GitHub know](https://github.com/search?q=%22io.changenow.changenow%22)
this app, so it's at best closed source and thus **not verifiable**.
