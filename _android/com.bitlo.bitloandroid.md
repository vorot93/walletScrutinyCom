---
wsId: bitlo
title: 'Bitlo: Bitcoin & Kripto Para'
altTitle: 
authors:
- danny
users: 100000
appId: com.bitlo.bitloandroid
appCountry: TR
released: 2020-12-18
updated: 2023-04-28
version: 2.1.0
stars: 2.7
ratings: 
reviews: 785
size: 
website: https://www.bitlo.com/
repository: 
issue: 
icon: com.bitlo.bitloandroid.png
bugbounty: 
meta: ok
verdict: custodial
date: 2023-02-08
signer: 
reviewArchive: 
twitter: bitlocom
social:
- https://www.linkedin.com/company/bitlo/
redirect_from: 
developerName: Bitlo Teknoloji A.Ş.
features: 

---

## App Description 

This is primarily a Turkish cryptocurrency exchange app. Once the app is installed, the app asks you to register. We were not able to register as it needed a Turkish ID number/Turkish foreigner number. 

> Your digital coins are stored in offline wallets. In our high security system, your investments are always safe.

## Analysis 

As a cryptocurrency exchange that stores coins in cold storage on behalf of customers, this is a **custodial** service.
