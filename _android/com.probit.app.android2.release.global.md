---
wsId: 
title: 'ProBit Global: Buy BTC, Crypto'
altTitle: 
authors:
- leo
users: 500000
appId: com.probit.app.android2.release.global
appCountry: 
released: 2019-06-19
updated: 2023-06-01
version: 1.50.5
stars: 4.4
ratings: 6161
reviews: 319
size: 
website: https://www.probit.com
repository: 
issue: 
icon: com.probit.app.android2.release.global.png
bugbounty: 
meta: ok
verdict: custodial
date: 2020-11-28
signer: 
reviewArchive: 
twitter: ProBit_Exchange
social:
- https://www.linkedin.com/company/probit-exchange
- https://www.facebook.com/probitexchange
redirect_from:
- /com.probit.app.android2.release.global/
developerName: Probit Global Services Limited
features: 

---

Probit appears to also and mainly be an exchange and as we can't find claims to
the contrary, we assume this app is a custodial offering and thus **not verifiable**.
