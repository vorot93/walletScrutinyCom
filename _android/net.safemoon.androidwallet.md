---
wsId: safemoon
title: SafeMoon
altTitle: 
authors:
- danny
users: 100000
appId: net.safemoon.androidwallet
appCountry: 
released: 2021-09-10
updated: 2023-06-08
version: V3.44
stars: 4.3
ratings: 
reviews: 6409
size: 
website: https://safemoon.com
repository: 
issue: 
icon: net.safemoon.androidwallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-02-13
signer: 
reviewArchive: 
twitter: safemoon
social: 
redirect_from: 
developerName: SafeMoon
features: 

---

## App Description 

The Safemoon wallet supports assets from the following: 

- ERC20 (Ethereum)
- BEP20 (Binance)
- Polygon
- Avalanche
- Solana

## Analysis 

This wallet does not have Bitcoin support. 
