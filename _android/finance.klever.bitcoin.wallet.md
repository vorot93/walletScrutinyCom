---
wsId: 
title: 'Klever Wallet K5: Crypto, BTC'
altTitle: 
authors: 
users: 50000
appId: finance.klever.bitcoin.wallet
appCountry: 
released: 2023-01-16
updated: 2023-06-07
version: 5.18.1
stars: 5
ratings: 
reviews: 23
size: 
website: https://klever.org/
repository: 
issue: 
icon: finance.klever.bitcoin.wallet.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-06-03
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: 'Klever Wallet: Bitcoin, Ethereum, TRX & Crypto'
features: 

---

