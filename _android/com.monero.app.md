---
wsId: 
title: Monero.com
altTitle: 
authors:
- danny
users: 10000
appId: com.monero.app
appCountry: 
released: 2022-01-07
updated: 2023-05-26
version: 1.3.7
stars: 4.2
ratings: 
reviews: 11
size: 
website: https://cakewallet.com
repository: 
issue: 
icon: com.monero.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-04-28
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Cake Labs
features: 

---

## App Description from [Google Play](https://play.google.com/store/apps/details?id=com.monero.app) 

> Monero.com is a Monero (XMR) only wallet. Monero.com allows you to safely store, exchange, and spend your Monero. Monero.com is focused on an excellent transaction experience.

## Analysis 

The app does not support BTC.