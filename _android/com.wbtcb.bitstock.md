---
wsId: bitPlusbyWBTCb
title: Bit.plus
altTitle: 
authors:
- danny
users: 10000
appId: com.wbtcb.bitstock
appCountry: 
released: 2020-06-17
updated: 2023-06-06
version: 2.6.2
stars: 3.4
ratings: 
reviews: 1
size: 
website: https://www.futured.app/
repository: 
issue: 
icon: com.wbtcb.bitstock.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-04-13
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: IP wBTCb solutions, s.r.o.
features: 

---

{{ page.title }} is also a provider of Bitcoin ATMs in their region. 

## App Description from [Google Play](https://play.google.com/store/apps/details?id=com.wbtcb.bitstock) 

> Bit.plus is a very intuitive app to buy/sell and manage your Bitcoins, Gold and Ethereum. It offers a very simple and safe way to receive, send and trade cryptocurrency and Gold instantly. It features:
> - Biometrics login
> - Chart of BTC / Gold / ETH and your portfolio
> - Interaction with Bitcoin / Gold ATMs
> - Multiple types of notifications

## Google Reviews 

> [Harry Roozendaal](https://play.google.com/store/apps/details?id=com.wbtcb.bitstock&gl=us)<br>
  ★☆☆☆☆ October 13, 2022 <br>
       Avoid. Send money and nothing. When I try to get cash says performijg verification forever....

## Analysis 

We tried to register with the service but were not able to receive the confirmation sms. 

The company is also a Bitcoin ATM provider operating mostly in the Czech region. 

Without access to the main app, we would have to pause this specific review until more information is available. 

