---
wsId: BinanceUS
title: 'Binance.US: Buy Bitcoin & ETH'
altTitle: 
authors:
- leo
users: 1000000
appId: com.binance.us
appCountry: 
released: 2019-12-23
updated: 2023-06-03
version: 3.2.0
stars: 3.9
ratings: 7187
reviews: 5837
size: 
website: https://www.binance.us
repository: 
issue: 
icon: com.binance.us.png
bugbounty: 
meta: ok
verdict: custodial
date: 2020-11-17
signer: 
reviewArchive: 
twitter: binanceus
social:
- https://www.linkedin.com/company/binance-us
- https://www.facebook.com/BinanceUS
redirect_from:
- /com.binance.us/
developerName: BinanceUS
features: 

---

Binance being a big exchange, the description on Google Play only mentions
security features like FDIC insurance for USD balance but no word on
self-custody. Their website is not providing more information neither. We
assume the app is a custodial offering and therefore **not verifiable**.
