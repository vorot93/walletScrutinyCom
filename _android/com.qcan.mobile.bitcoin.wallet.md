---
wsId: Qcan
title: Mobile Bitcoin Wallet
altTitle: 
authors:
- leo
users: 10000
appId: com.qcan.mobile.bitcoin.wallet
appCountry: 
released: 2017-08-06
updated: 2023-04-24
version: 0.8.855
stars: 4.6
ratings: 166
reviews: 12
size: 
website: https://qcan.com
repository: 
issue: 
icon: com.qcan.mobile.bitcoin.wallet.png
bugbounty: 
meta: ok
verdict: nosource
date: 2020-12-08
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from:
- /com.qcan.mobile.bitcoin.wallet/
developerName: Qcan.com
features: 

---

> **Complete Control**<br>
  Your Bitcoin, 100% Under Your Control. You hold the key. No intermediary.

That is a clear claim to be non-custodial but neither on Google Play nor the
website can we find a link to source code. This app is thus **not verifiable**.
