---
wsId: bacoorhbwallet
title: HB Wallet | ETH Wallet, DeFi
altTitle: 
authors:
- danny
users: 100000
appId: co.bacoor.android.hbwallet
appCountry: us
released: 2017-07-17
updated: 2022-06-13
version: 3.6.0
stars: 4.5
ratings: 2137
reviews: 48
size: 
website: https://www.hb-wallet.com/
repository: 
issue: 
icon: co.bacoor.android.hbwallet.png
bugbounty: 
meta: stale
verdict: nobtc
date: 2023-06-10
signer: 
reviewArchive: 
twitter: HBWallet_Ether
social: 
redirect_from: 
developerName: bacoor inc.
features: 

---

We downloaded the app. This is an Ethereum wallet meant for Ethereum transactions. It is possible to buy Bitcoin, but through a third party provider. **No Bitcoins** are stored on this wallet.

