---
wsId: 
title: HandCash
altTitle: 
authors: 
users: 100000
appId: io.handcash.wallet
appCountry: 
released: 2019-09-10
updated: 2023-06-07
version: 5.2.1
stars: 4.6
ratings: 581
reviews: 90
size: 
website: https://handcash.io
repository: 
issue: 
icon: io.handcash.wallet.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2019-12-28
signer: 
reviewArchive: 
twitter: handcashapp
social: 
redirect_from:
- /io.handcash.wallet/
- /posts/io.handcash.wallet/
developerName: Handcash Tech
features: 

---

A BSV wallet.
