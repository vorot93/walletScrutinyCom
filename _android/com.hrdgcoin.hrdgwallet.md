---
wsId: 
title: HRDG Wallet
altTitle: 
authors:
- danny
users: 1000
appId: com.hrdgcoin.hrdgwallet
appCountry: 
released: 2021-07-09
updated: 2022-07-28
version: 1.0.8
stars: 
ratings: 
reviews: 
size: 
website: 
repository: 
issue: 
icon: com.hrdgcoin.hrdgwallet.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-05-26
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: TabiPay
features: 

---

## App Description from Google Play 

> HRDG Wallet is a cold wallet for Bitcoin, Ethereum, HRDG coin.
> - The key of wallet is kept in a secure storage and is not transmitted anywhere.
- HRDG wallet uses only one seed for all coins.
- It is recommended to keep recovery seed safely.

## Analysis 

- We tested the app and found a BTC app that can send and receive. 
- The app also provided a mnemonic phrase
- It does **not make any claims to be source available**.