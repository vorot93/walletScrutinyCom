---
wsId: bit2me
title: 'Bit2Me: Bitcoin and +200 more'
altTitle: 
authors:
- leo
users: 100000
appId: com.phonegap.bit2me
appCountry: 
released: 2015-01-08
updated: 2023-06-06
version: 2.11.1
stars: 4.2
ratings: 5162
reviews: 82
size: 
website: https://bit2me.com
repository: 
issue: 
icon: com.phonegap.bit2me.png
bugbounty: 
meta: ok
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive: 
twitter: bit2me
social:
- https://www.linkedin.com/company/bit2me
- https://www.facebook.com/bit2me
redirect_from:
- /com.phonegap.bit2me/
- /posts/com.phonegap.bit2me/
developerName: Bit2Me
features: 

---

This appears to be the interface for an exchange. We could not find any claims
about you owning your keys. As a custodial service it is **not verifiable**.
