---
wsId: 
title: 'Hippo: Crypto & Bitcoin Wallet'
altTitle: 
authors: 
users: 100000
appId: com.blockchaincommodities.hippo_wallet
appCountry: 
released: 2022-02-05
updated: 2023-06-07
version: 1.12.0
stars: 4.1
ratings: 
reviews: 16
size: 
website: https://hippowallet.io/
repository: 
issue: 
icon: com.blockchaincommodities.hippo_wallet.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-06-02
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Blockchain Commodities
features: 

---

