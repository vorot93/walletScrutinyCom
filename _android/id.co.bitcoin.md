---
wsId: indodax
title: Indodax Buy Sell Crypto Assets
altTitle: 
authors:
- leo
users: 1000000
appId: id.co.bitcoin
appCountry: 
released: 2014-12-15
updated: 2023-06-06
version: 5.0.6
stars: 4.2
ratings: 97568
reviews: 457
size: 
website: http://indodax.com
repository: 
issue: 
icon: id.co.bitcoin.png
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-31
signer: 
reviewArchive: 
twitter: indodax
social: 
redirect_from: 
developerName: Indodax
features: 

---

This is the interface to

> Indodax is Indonesia’s largest crypto asset marketplace

and on their website they claim:

> **Security**<br>
  Every transaction is protected with Multi-factor Authentication, combining
  email verification and Google Authenticator SMS to guarantee that your
  transaction is truly signed and validated only by you.

With no further explanation and as this is an exchange, we assume the app is a
custodial offering and thus **not verifiable**.