---
wsId: 
title: Krypterz
altTitle: 
authors:
- danny
users: 1000
appId: com.app.exchangejl
appCountry: 
released: 2022-03-29
updated: 2023-05-22
version: 1.5.0
stars: 
ratings: 
reviews: 
size: 
website: https://www.krypterz.com
repository: 
issue: 
icon: com.app.exchangejl.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-05-11
signer: 
reviewArchive: 
twitter: Krypterzxchange
social:
- https://www.facebook.com/Krypterz-101716118890363
- https://www.linkedin.com/company/krypterz
- mailto:info@krypterz.com
redirect_from: 
developerName: Krypterz Exchange
features: 

---

## App Description from Google Play 

> Join us and start trading Carbon Credits (KPZ), Bitcoin (BTC), Ethereum (ETH), Shiba Inu (SHIB), DOGE, etc.

## Analysis 

- We installed the app and was greeted with a message that reads:

> This app has been temporarily disabled by the publisher

- We sent an email to info@krypterz.com 
- For now, we'll keep this as a **work-in-progress** until more information is made available.