---
wsId: 
title: CFD Trading Forex investing ap
altTitle: 
authors:
- danny
users: 5000
appId: com.faerlik.firewin
appCountry: 
released: 
updated: 2021-10-11
version: '0.2'
stars: 
ratings: 
reviews: 
size: 
website: 
repository: 
issue: 
icon: com.faerlik.firewin.png
bugbounty: 
meta: stale
verdict: nowallet
date: 2023-04-22
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: AIGUL WIN
features: 

---

## App Description from Google Play 

> After installing the application, you can immediately start trading in the terminal. Many users have already chosen our online trading platform to buy and sell CFDs on stocks, stocks, ETFs, cryptocurrencies and indices. Our application is not a broker bitcoin or a Forex, it is a completely free application for familiarization with trading.

## Analysis 

We installed the app [(Screenshots)](https://twitter.com/BitcoinWalletz/status/1649620051908263937), and found a bare interface with no option to register, sign-in, deposit or withdraw. This is **not a wallet at all**. 
