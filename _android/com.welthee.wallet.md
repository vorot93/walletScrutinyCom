---
wsId: weltheeWallet
title: Welthee Wallet
altTitle: 
authors:
- danny
users: 10000
appId: com.welthee.wallet
appCountry: 
released: 2021-09-07
updated: 2023-05-22
version: 4.4.0
stars: 
ratings: 
reviews: 
size: 
website: https://welthee.com/
repository: 
issue: 
icon: com.welthee.wallet.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-04-13
signer: 
reviewArchive: 
twitter: Welthee
social:
- https://www.linkedin.com/company/welthee/
redirect_from: 
developerName: Capitera AG
features: 

---

## App Description from Google Play 

> Welthee Wallet is the official wallet of the Welthee investment platform.

> Supported cryptocurrencies
>
> - Welthee (WELT)
> - Ethereum (ETH)
> - Matic - Polygon
> - USDC (Polygon)
> - Launchpad tokens
> - RiftOne (RFT)
> - Dexvers(DXVS)
> - TOKHITT (HITT)
> - Superkoin (SK)

## Analysis 

{{ page.title }} does not list BTC among its supported currencies.