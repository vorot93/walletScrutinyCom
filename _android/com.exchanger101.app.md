---
wsId: 
title: Exchanger101
altTitle: 
authors:
- danny
users: 1000
appId: com.exchanger101.app
appCountry: 
released: 2021-09-30
updated: 2023-04-16
version: 1.15.27
stars: 4.4
ratings: 
reviews: 5
size: 
website: https://exchanger101.com
repository: 
issue: 
icon: com.exchanger101.app.png
bugbounty: 
meta: ok
verdict: custodial
date: 2023-05-24
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: exchanger company
features: 

---

## App Description from Google Play 

> Exchanger101 is the all-in-one trading App that helps you convert your Giftcards and cryptocurrencies to Naira

## Analysis 

- [Screenshots](https://twitter.com/BitcoinWalletz/status/1661255161518637056)
- We successfully installed the app and registered
- The app had a BTC wallet that can be sold/bought, and, can be sent/received. 
- The option to backup the private keys was not provided. 
- The app also links to tradfi and other banking related features.
- This app is **custodial**.
