---
wsId: 99Recharge
title: 99pay Mobile, 00301 recharge
altTitle: 
authors:
- leo
users: 100000
appId: com.qqtrade.gugupay
appCountry: cn
released: 2017-05-25
updated: 2023-05-30
version: 5.0.4
stars: 4.2
ratings: 658
reviews: 253
size: 
website: http://www.99pay.kr/
repository: 
issue: 
icon: com.qqtrade.gugupay.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-12-26
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: (주)구구페이
features: 

---

You probably are looking for the other 99pay app:

{% include walletLink.html wallet='android/com.pay99.wallet' verdict='true' %}  