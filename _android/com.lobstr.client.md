---
wsId: lobstrco
title: LOBSTR Wallet. Buy Stellar XLM
altTitle: 
authors:
- danny
users: 1000000
appId: com.lobstr.client
appCountry: us
released: 2015-04-27
updated: 2023-06-08
version: 9.4.1
stars: 4.6
ratings: 11618
reviews: 1727
size: 
website: https://lobstr.co/
repository: 
issue: 
icon: com.lobstr.client.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-08-27
signer: 
reviewArchive: 
twitter: Lobstrco
social: 
redirect_from: 
developerName: LOBSTR Stellar Wallet
features: 

---

As the description states, lobstr is a stellar lumens wallet. 

However, looking deeper you'll find that you can purchase other assets including bitcoin. You can also add that to the "Assets". 

You need to deposit 5 XLM in order to add the BTC asset.

However, lobstr presumably only holds btc tokens and not actual bitcoin. Hence, the **nobtc** verdict.

Zendesk support page "[Buying Crypto with LOBSTR wallet](https://lobstr.zendesk.com/hc/en-us/articles/360014741460-Buying-crypto-with-LOBSTR-wallet)"

