---
wsId: 
title: Trezor Suite Lite
altTitle: 
authors: 
users: 5000
appId: io.trezor.suite
appCountry: 
released: 2023-05-09
updated: 2023-05-09
version: 23.5.1
stars: 4.3
ratings: 
reviews: 4
size: 
website: https://trezor.io
repository: 
issue: 
icon: io.trezor.suite.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-06-03
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Trezor
features: 

---

