---
wsId: 
title: Paribu | Bitcoin - Kripto Para
altTitle: 
authors:
- danny
users: 5000000
appId: com.paribu.app
appCountry: 
released: 2019-06-14
updated: 2023-05-05
version: 4.0.5
stars: 2.5
ratings: 73042
reviews: 86
size: 
website: https://www.paribu.com
repository: 
issue: 
icon: com.paribu.app.png
bugbounty: 
meta: ok
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive: 
twitter: paribucom
social:
- https://www.linkedin.com/company/paribu
- https://www.facebook.com/paribucom
redirect_from: 
developerName: PARİBU
features: 

---

The app's description is written in Turkish, so all the quotes below are from Google Translate.

> In Paribu, user assets are stored in secure cold wallets. You can always perform your transactions safely with up-to-date security measures.

This sounds custodial.
