---
wsId: 
title: DFX Bitcoin Wallet
altTitle: 
authors: 
users: 100
appId: com.defichain.app.dfx.bitcoin
appCountry: 
released: 2023-02-23
updated: 2023-04-13
version: 2.1.0
stars: 
ratings: 
reviews: 
size: 
website: https://dfx.swiss/bitcoin/
repository: 
issue: 
icon: com.defichain.app.dfx.bitcoin.png
bugbounty: 
meta: ok
verdict: fewusers
date: 2023-06-03
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: DFX AG
features: 

---

