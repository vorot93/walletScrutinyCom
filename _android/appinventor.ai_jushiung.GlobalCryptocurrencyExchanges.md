---
wsId: 
title: Global Cryptocurrency Exchange
altTitle: 
authors:
- danny
users: 1000
appId: appinventor.ai_jushiung.GlobalCryptocurrencyExchanges
appCountry: 
released: 2019-11-12
updated: 2019-11-12
version: '1.0'
stars: 
ratings: 
reviews: 
size: 
website: 
repository: 
issue: 
icon: appinventor.ai_jushiung.GlobalCryptocurrencyExchanges.png
bugbounty: 
meta: obsolete
verdict: nowallet
date: 2023-05-09
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: CAD Design
features: 

---

- The app only shows icons linking to popular exchanges
- It has no other function and thus, no support for wallets.