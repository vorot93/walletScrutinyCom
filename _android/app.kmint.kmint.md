---
wsId: 
title: KMINT:All-in-One Crypto Wallet
altTitle: 
authors:
- danny
users: 10000
appId: app.kmint.kmint
appCountry: 
released: 2022-03-03
updated: 2023-05-31
version: 1.0.16
stars: 3.8
ratings: 
reviews: 3
size: 
website: https://www.kmintprotocol.io/
repository: 
issue: 
icon: app.kmint.kmint.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-03-07
signer: 
reviewArchive: 
twitter: KMINT_protocol
social:
- https://t.me/KMINT_protocol
- https://kmint-protocol.medium.com/
redirect_from: 
developerName: BPMG Inc
features: 

---

## App Description 

> 1. All-in-One Multi-Chain Wallet
> Inconvenience with using different wallets by mainnet is resolved.
> All blockchain services can be easily used within one app.

The app is compatible with MetaMask

## Analysis 

While the seed phrases were generated upon app initialization, we were not able to add and find 
Bitcoin (BTC) as one of the supported coins. There is a preponderence of "btc" but as tokenized versions 
on other chains namely: 

- Binance Chain 
- Polygon Chain 
- Klaytn Chain 
- Ethereum Chain 

This app does not support BTC.