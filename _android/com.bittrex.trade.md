---
wsId: bittrex
title: Bittrex | Buy Bitcoin & Crypto
altTitle: 
authors:
- leo
users: 500000
appId: com.bittrex.trade
appCountry: 
released: 2019-12-19
updated: 2023-06-05
version: 1.22.2
stars: 3.9
ratings: 5144
reviews: 649
size: 
website: https://global.bittrex.com
repository: 
issue: 
icon: com.bittrex.trade.png
bugbounty: 
meta: ok
verdict: custodial
date: 2021-02-09
signer: 
reviewArchive: 
twitter: BittrexGlobal
social:
- https://www.facebook.com/BittrexGlobal
redirect_from: 
developerName: Bittrex, Inc.
features: 

---

This app is an interface to a trading platform:

> The Bittrex Global mobile app allows you to take the premiere crypto trading
  platform with you wherever you go.

As such, it lets you access your account with them but not custody your own
coins and therefore is **not verifiable**.
