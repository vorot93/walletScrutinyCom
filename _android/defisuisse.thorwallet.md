---
wsId: 
title: THORWallet - DeFi Wallet
altTitle: 
authors: 
users: 10000
appId: defisuisse.thorwallet
appCountry: 
released: 2021-12-23
updated: 2023-05-30
version: 2.0.4
stars: 4.3
ratings: 
reviews: 18
size: 
website: http://thorwallet.org
repository: 
issue: 
icon: defisuisse.thorwallet.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-06-02
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: DeFi Suisse AG
features: 

---

