---
wsId: 
title: Privateum Wallet
altTitle: 
authors:
- danny
users: 1000
appId: com.privateum.wallet
appCountry: 
released: 2021-12-14
updated: 2023-02-28
version: 2.0.22
stars: 5
ratings: 
reviews: 4
size: 
website: https://privateum.com/
repository: 
issue: 
icon: com.privateum.wallet.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-05-31
signer: 
reviewArchive: 
twitter: privateumglobal
social:
- https://www.facebook.com/privateumglobal
- https://www.linkedin.com/company/privateumglobal
- https://www.instagram.com/privateumglobal
- https://www.reddit.com/r/Privateum
- https://privateumglobal.medium.com
- https://t.me/PRIVATEUMGLOBAL
redirect_from: 
developerName: Privateum
features: 

---

## App Description from Google Play 

> Privateum Wallet is the official digital assets and crypto wallet of Privateum Initiative platform. You can send, receive and store Bitcoin and many other cryptocurrencies and digital assets safely and securely with the Privateum Wallet mobile app.
>
> Keep your private key secure with bank-level security

## Analysis 

- There is a link to an Apple store app, but leads to a blank page.
- We were given the option to Create a New Wallet or Add Existing Wallet
- We were given the 12-word mnemonics 
- We initially saw $PVM, $PRI, $USDT and $BNB 
- Tapping on 'Manage Networks' where the default is Binance Chain, leads us to a blank screen with no other option. This disallows the usage of the Bitcoin chain, and thus, Bitcoin support. 
- We tweeted [@privateum](https://twitter.com/BitcoinWalletz/status/1663859816270376961) about this potential bug.
- This could go either of two ways: no source-availability or no support for BTC. For now, we'll keep this as a **work-in-progress** until they respond.