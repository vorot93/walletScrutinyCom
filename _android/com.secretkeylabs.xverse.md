---
wsId: xverse
title: Xverse - Bitcoin Wallet
altTitle: 
authors:
- danny
users: 50000
appId: com.secretkeylabs.xverse
appCountry: cn
released: 2021-10-12
updated: 2023-06-01
version: 1.17.1
stars: 4.1
ratings: 34
reviews: 481
size: 
website: https://www.xverse.app/
repository: 
issue: 
icon: com.secretkeylabs.xverse.png
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-13
signer: 
reviewArchive: 
twitter: secretkeylabs
social: 
redirect_from: 
developerName: Secret Key Labs
features: 

---

## App Description

> Send & receive Stacks (STX), Bitcoin (BTC) and all tokens built on Stacks. Xverse wallet is a mobile wallet for the Stacks blockchain.

## Verdict

After installing the app, we created a 6-digit pin code. We were also provided with a 12-word mnemonic phrase. 

Backing up the wallet is also possible. However, once we loaded the app, the Bitcoin wallet can't seem to load from the interface. 

Judging from the Google Play pictures, there should be a BTC wallet. 

We could **not find any information indicating whether the project is open source.** 


  